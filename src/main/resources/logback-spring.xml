<?xml version="1.0" encoding="UTF-8"?>
<!--
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    LOG LEVEL INFO

    ① FATAL : 가장 크리티컬한 에러가 일어 났을 때 사용합니다.
    ② ERROR : 일반 에러가 일어 났을 때 사용합니다.
    ③ WARN  : 에러는 아니지만 주의할 필요가 있을 때 사용합니다.
    ④ INFO  : 일반 정보를 나타낼 때 사용합니다.
    ⑤ DEBUG : 일반 정보를 상세히 나타낼 때 사용합니다.

    만약 로깅 레벨을 WARN 으로 설정하였다면 그 이상 레벨만 로깅하게 됩니다.
    즉 WARN, ERROR, FATAL 의 로깅이 됩니다.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-->


<!--
========================================================================
    30초마다 설정 파일의 변경을 확인한다. 파일이 변경되면 다시 로딩한다
========================================================================
-->
<configuration scan="true" scanPeriod="30 seconds">

    <!--
    ==========================================================================
        로그파일 저장 경로 Property 설정
    ==========================================================================
     -->
    <springProperty scope="context" name="LOG_FILE_PATH"    source="config.logging.path" />
    <springProperty scope="context" name="LOG_FILE_NAME"    source="config.logging.name" />
    <springProperty scope="context" name="LOG_FILE_PATTERN" source="config.logging.pattern" />
    <springProperty scope="context" name="LOG_LEVEL"        source="config.logging.level.src" />
    <springProperty scope="context" name="LOG_LEVEL_JDBC"   source="config.logging.level.jdbc" />

    <!-- pattern -->
<!--    <property name="LOG_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss} %d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"/>-->
    <property name="LOG_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss} %-4relative [%thread] %-5level %logger{35} - %msg%n"/>

    <!--
    ==========================================================================
        Console 출력 설정
    ==========================================================================
     -->
    <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>${LOG_PATTERN}</pattern>
        </encoder>
    </appender>


    <!--
    ==========================================================================
        File 출력 설정
    ==========================================================================
    -->
    <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">

        <!-- 파일경로 설정 -->
        <file>${LOG_FILE_PATH}/${LOG_FILE_NAME}.log</file>

        <!-- 출력패턴 설정 -->
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>${LOG_PATTERN}</pattern>
        </encoder>

        <!-- 파일이 하루에 한개씩 생성된다 -->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- 파일 확장자를 zip으로 선언하면 압축하여 생성 됨. -->
            <fileNamePattern>${LOG_FILE_PATH}/${LOG_FILE_NAME}.%d{yyyy-MM-dd}_%i..${LOG_FILE_PATTERN}</fileNamePattern>

            <!-- 로그파일 용량으로 분할(대소문자 구분X) - kb, mb, gb -->
            <timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
                <maxFileSize>100MB</maxFileSize>
            </timeBasedFileNamingAndTriggeringPolicy>

            <!-- 로그파일 삭제 (60일 초과 경과된 파일) -->
            <maxHistory>60</maxHistory>

        </rollingPolicy>

        <!-- 기존파일에 추가할지 설정 -->
        <append>true</append>

        <!-- file에 저장될때 lock을 생성해서 처리. 서로 다른 java vm이 같은 파일을 가리킬때 사용 -->
        <prudent>false</prudent>

    </appender>


    <!--
    =====================================================================
        Root Logger Level 설정
    =====================================================================
    -->
    <root level="INFO">
        <appender-ref ref="CONSOLE"/>
        <appender-ref ref="FILE"/>
    </root>

    <!--
    =====================================================================
        패키지별 Logger Level 설정
    =====================================================================
    -->

    <!-- Spring Loggers -->
    <!-- 화면 비우기를 방지하는 데 사용되는 타사 패키지의 로그 필터링 수준 -->
    <logger name="net.sf.ehcache" level="WARN" />
    <logger name="org.jboss.logging" level="WARN" />
    <logger name="com.alibaba" level="WARN" />
    <logger name="druid.sql.Connection" level="WARN" />
    <logger name="org.hibernate" level="WARN" />
    <logger name="org.springframework" level="WARN" />
    <logger name="org.apache" level="WARN" />

    <!-- log4jdbc 옵션 설정 -->
    <!--
        - jdbc.connection       : 커넥션 open close 이벤트를 로그로 남긴다.
        - jdbc.sqlonly          : SQL문만을 로그로 남기며, PreparedStatement일 경우 관련된 argument 값으로 대체된 SQL문이 보여진다.
        - jdbc.sqltiming        : SQL문과 해당 SQL을 실행시키는데 수행된 시간 정보(milliseconds)를 포함한다.
        - jdbc.audit            : ResultSet을 제외한 모든 JDBC 호출 정보를 로그로 남긴다. 많은 양의 로그가 생성되므로 특별히 JDBC 문제를 추적해야 할 필요가 있는 경우를 제외하고는 사용을 권장하지 않는다.
        - jdbc.resultset        : ResultSet을 포함한 모든 JDBC 호출 정보를 로그로 남기므로 매우 방대한 양의 로그가 생성된다.
        - jdbc.resultsettable   : SQL 결과 조회된 데이터의 table을 로그로 남긴다.
    -->
    <logger name="jdbc" level="OFF" />
    <logger name="jdbc.connection" level="OFF" />
    <logger name="jdbc.sqlonly" level="OFF" />
    <logger name="jdbc.sqltiming" level="${LOG_LEVEL_JDBC}" />
    <logger name="jdbc.audit" level="OFF" />
    <logger name="jdbc.resultset" level="OFF" />
    <logger name="jdbc.resultsettable" level="${LOG_LEVEL_JDBC}" />

    <logger name="ProxyPreparedStatement" level="OFF" />


    <!-- Package Logger -->
    <logger name="com.toady" level="${LOG_LEVEL_SRC}" />

    <!-- Hibernate SQL 구성 -->
    <logger name="org.hibernate.type.descriptor.sql.BasicBinder" level="OFF" />
    <logger name="p6spy" level="OFF" />

</configuration>


