package com.toady.web.core.pagination;

import com.toady.web.core.constants.CmnConst;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Setter
public class PageDTO {

	@Getter
    @Setter
    @NotNull
    @Min(0)
    private int pageNumber;
    
    @Getter
    @Setter
    @NotNull
    @Min(0)
    @Builder.Default 
    private int recordPerCount = CmnConst.RECORD_PER_COUNT;

	public int getPageNumber() {
		return pageNumber-1;
	}
}
