package com.toady.web.core.exception;


import org.springframework.http.HttpStatus;

/**
 * @author youn
 */
public class BaseException extends RuntimeException {

    private HttpStatus status;

    public BaseException(String resultMessage) {
        super(resultMessage);
    }

    public BaseException(String resultMessage, HttpStatus status) {
        super(resultMessage);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
