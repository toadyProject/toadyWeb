package com.toady.web.core.security.jwt.filter;


import com.toady.web.core.security.jwt.JwtProvider;
import io.jsonwebtoken.ClaimJwtException;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * JWT 토큰 검증.
 * @author youn
 */
@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE) // 파라미터가 없는 기본 생성자 생성.
public class JwtAuthenticationFilter extends GenericFilterBean {

	private JwtProvider jwtProvider;

	public JwtAuthenticationFilter(JwtProvider jwtProvider) {
		this.jwtProvider = jwtProvider;
	}

	@Override
	public void doFilter(ServletRequest servletRequest,
						 ServletResponse servletResponse,
						 FilterChain filterChain
	) throws IOException, ServletException, ExpiredJwtException, ClaimJwtException {

		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

		try {

			//토큰이 없다면.
			if(httpServletRequest.getHeader("Authorization") == null){
				log.info("token is null");
			}else {

				Authentication authentication = jwtProvider.getAuthentication(httpServletRequest);

				if (authentication != null) {

					SecurityContextHolder.getContext().setAuthentication(authentication);

					if(jwtProvider.getJwtExpiryRenewalCheck(httpServletRequest, authentication)){
						//토큰 생성.
						String token = jwtProvider.generatorToken(authentication);

						//생성한 토큰을 헤더에 넣는다.
						httpServletResponse.setHeader("Authorization", token);
					}else{
						//기존 토큰을 넣는다.
						httpServletResponse.setHeader("Authorization", httpServletRequest.getHeader("Authorization"));
					}


				} else {
					SecurityContextHolder.clearContext();
				}
			}

		} catch (ExpiredJwtException e) {
			httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED); //401 error.
			SecurityContextHolder.clearContext();
			log.error("토큰 만료 에러 : {}, {}", e.getMessage(), e);
			throw new ExpiredJwtException(null, null, "토큰 만료 에러");

		} catch (ClaimJwtException e) {
			httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED); //401 error.
			SecurityContextHolder.clearContext();
			log.error("토큰 에러 : {}, {}", e.getMessage(), e);
			throw new ExpiredJwtException(null, null, "토큰 만료 에러");
		} catch (Exception e) {
			httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED); //401 error.
			SecurityContextHolder.clearContext();
			log.error("시스템 에러 : {}, {}", e.getMessage(), e);
		} finally {
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}


}
