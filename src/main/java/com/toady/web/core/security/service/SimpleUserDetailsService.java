package com.toady.web.core.security.service;


import com.toady.web.app.user.entity.UserEntity;
import com.toady.web.app.user.repository.UserRepository;
import com.toady.web.core.constants.AuthorityCd;
import com.toady.web.core.util.SecUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 로그인 검증.
 *
 * @author youn
 */
@Log4j2
@Component
public class SimpleUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String userEmail)
			throws UsernameNotFoundException,
			       CredentialsExpiredException,
			       DisabledException,
			       AccountExpiredException,
			       LockedException
	{

		log.info("로그인 시도 [userEmail : "+userEmail+"] DB 조회.");
		UserEntity dbUserInfo = userRepository.findByUserEmail(userEmail);
		
		if (dbUserInfo == null) {
			log.info("NOT Found User"); //일치하는 정보가 없습니다.
			throw new UsernameNotFoundException("Email 또는 Password가 잘못되었습니다.");
		}

		if ("Y".equals(dbUserInfo.getLockYn())) { //계정 락 체크
			throw new LockedException("계정이 잠겨 있습니다.");
		}

		if ("Y".equals(dbUserInfo.getWithdrawalYn())) { //탈퇴 회원 체크
			throw new AccountExpiredException("탈퇴한 회원입니다.");
		}

		if(!"Y".equals(dbUserInfo.getEmailCertifyYn())){
			throw new DisabledException("인증 요청된 Email을 확인 해 주세요.<br/>인증 후 로그인 가능합니다.");
		}

		List<String> authList = new ArrayList();
		authList.add(AuthorityCd.USER.roleCd()); //권한그룹이 존재하니 임의로 ADMIN을 넣어줌.

		CustomUserDetails user = new CustomUserDetails(dbUserInfo.getUserEmail(), dbUserInfo.getUserPassword(), SecUtils.mapToGrantedAuthorities(authList));
		user.setUserSeq(dbUserInfo.getUserSeq());
		user.setUserNicName(dbUserInfo.getUserNicName());

		//return new User(dbUserInfo.getUserEmail(), dbUserInfo.getUserPassword(), SecUtils.mapToGrantedAuthorities(authList));
		return user;
	}
}
