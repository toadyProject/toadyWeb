package com.toady.web.core.security.jwt.impl;


import com.toady.web.core.security.jwt.JwtProvider;
import com.toady.web.core.security.service.CustomUserDetails;
import com.toady.web.core.util.SecUtils;
import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * jwt 토큰 생성.
 *
 * @author youn
 */
@Log4j2
public class JwtProviderImpl implements JwtProvider, InitializingBean {


	@Value("${toady.security.SECRET_KEY}")
	private String SECRET_KEY;

	@Value("${toady.security.JWT_PREFIX}")
	private String JWT_PREFIX;

	@Value("${toady.security.JWT_HEADER_STRING}")
	private String JWT_HEADER_STRING;

	@Value("${toady.security.AUTHORITIES_KEY_NM}")
	private String AUTHORITIES_KEY_NM;

	@Value("${toady.security.TOKEN_EXPIRE_MS}")
	private long TOKEN_EXPIRE_MS;

	@Value("${toady.security.TOKEN_CREATED_AT}")
	private String TOKEN_CREATED_AT;

	/**
	 * 토큰 생성
	 *
	 * @param authentication
	 * @return
	 */
	@Override
	public String generatorToken(Authentication authentication) {

		String authorities = SecUtils.getAuthorities(authentication.getAuthorities());
		Date expireDate = new Date(System.currentTimeMillis() + TOKEN_EXPIRE_MS);
		CustomUserDetails user =  (CustomUserDetails) authentication.getPrincipal();

		//@formatter:off
		return Jwts.builder()
			.setSubject(authentication.getName())

			.claim(AUTHORITIES_KEY_NM, authorities)
			.claim(TOKEN_CREATED_AT, new Date()) //생성일

			.claim("userSeq", user.getUserSeq())
			.claim("userNicName", user.getUserNicName())

			.signWith(SignatureAlgorithm.HS512, SECRET_KEY)
			.setExpiration(expireDate) //만료일
			.compact();
		//@formatter:on
	}

	/**
	 * 인증 정보 가져오기
	 */
	@Override
	public Authentication getAuthentication(HttpServletRequest request) throws ClaimJwtException, ExpiredJwtException {

		String fullToken = request.getHeader(JWT_HEADER_STRING);

		if (StringUtils.isBlank(fullToken)) {
			return null;
		}

		String jwt = StringUtils.replace(fullToken, JWT_PREFIX, "");

		if (StringUtils.isBlank(jwt)) {
			return null;
		}

		Claims claims = getJwtClaims(jwt);

		if (claims == null) {
			return null;
		}

		//jwt토큰시간이 만료됐는지 체크.
		if (claims.getExpiration().before(new Date())) {
			return null;
		}
		String authoritiesStr = claims.get(AUTHORITIES_KEY_NM).toString();

		Collection<? extends GrantedAuthority> authorities = SecUtils.mapToGrantedAuthorities(authoritiesStr);

		CustomUserDetails user = new CustomUserDetails(claims.getSubject(), "", authorities);
		user.setUserSeq(Long.parseLong(claims.getOrDefault("userSeq", "").toString()));
		user.setUserNicName(claims.getOrDefault("userNicName", "").toString());
		/*
		//String userEmail = claims.getSubject();
		UserEntity user = new UserEntity();
		user.setUserEmail(claims.getSubject());
		user.setUserSeq( Long.parseLong(claims.getOrDefault("userSeq", "").toString()) );
		user.setUserNicName(claims.getOrDefault("userNicName", "").toString());
		*/

		// Create login token
		return new UsernamePasswordAuthenticationToken(user, null, authorities);
	}

	/**
	 * jwt token 시간 만료가 30분이하로 남았으면 갱신한다.
	 * @param request
	 * @param authentication
	 * @return 갱신 해야하면 true, 갱신 안해도 되면 false.
	 */
	public boolean getJwtExpiryRenewalCheck(HttpServletRequest request, Authentication authentication){
		String fullToken = request.getHeader(JWT_HEADER_STRING);

		if (StringUtils.isBlank(fullToken)) {
			return false;
		}

		String jwt = StringUtils.replace(fullToken, JWT_PREFIX, "");

		if (StringUtils.isBlank(jwt)) {
			return false;
		}

		Claims claims = getJwtClaims(jwt);
		if (claims == null) {
			return false;
		}

		//jwt토큰시간이 만료됐는지 체크.
		if (claims.getExpiration().before(new Date())) {
			return false;
		}

		// 포맷변경 ( 년월일 시분초)
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		log.info("토큰에 저장된 만료시간. : " + sdformat.format(claims.getExpiration()));

		// Java 시간 더하기
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MINUTE, 30);// 30분 더하기
		Date date = new Date(cal.getTimeInMillis()); // Date(long date)

		//token 유효 시간이 현재시간으로부터 30분보다 적게 남아있다면 갱신한다.
		if(claims.getExpiration().before(date)){
			return true;
		}

		return false;
	}

	private Claims getJwtClaims(String token) throws ClaimJwtException, ExpiredJwtException {
		return Jwts.parser()
				.setSigningKey(SECRET_KEY)
				.parseClaimsJws(token)
				.getBody();
	}

	/**
	 * InintializingBean 인터페이스의 메소드로 BeanFactory에 의해 모든 property 가 설정되고 난 뒤 실행되는 메소드 
	 */
	@Override
	public void afterPropertiesSet() throws Exception {

	}
}
