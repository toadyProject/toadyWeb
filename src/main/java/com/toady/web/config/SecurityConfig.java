package com.toady.web.config;


import com.toady.web.core.security.jwt.JwtProvider;
import com.toady.web.core.security.jwt.filter.JwtAuthenticationFilter;
import com.toady.web.core.security.jwt.impl.JwtProviderImpl;
import com.toady.web.core.security.service.SimpleUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.servlet.http.HttpServletResponse;


/**
 * 시큐리티 설정
 *
 * @author youn
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private SimpleUserDetailsService simpleUserDetailsService;

	@Bean
	public JwtProvider jwtProvider() {
		return new JwtProviderImpl();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//@formatter:off
		http
			.addFilterBefore(new JwtAuthenticationFilter(jwtProvider()),
					              UsernamePasswordAuthenticationFilter.class
					              )
			.cors()
				//.configurationSource(corsConfigurationSource())
			.and()
				.csrf().disable()
			.authorizeRequests()
				.antMatchers("/webApi/user/login").permitAll()
			.anyRequest() //다른 모든 요청들을 인증이나 권한 없이 허용한다
				.permitAll()
			.and()
				.headers().frameOptions().disable()
			.and()
				.exceptionHandling()
					.authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)) ;
		//@formatter:on

		//super.configure(http);
	}

	@Override
	protected UserDetailsService userDetailsService() {
		return simpleUserDetailsService;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	// CORS 허용 적용
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();

		//configuration.addAllowedOrigin("*"); //허용할 URL
		configuration.addAllowedOriginPattern("*");
		configuration.addAllowedHeader("*"); //허용할 Header
		configuration.addAllowedMethod("*"); //허용할 Http Method
		configuration.setAllowCredentials(true);

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}
