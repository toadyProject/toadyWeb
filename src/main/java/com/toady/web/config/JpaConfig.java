package com.toady.web.config;

import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * packageName    : com.toady.web.config
 * fileName       : JpaConfig
 * author         : youn
 * date           : 2022/05/19
 * description    : jpa 설정 java config.
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */
@Configuration
@EnableJpaRepositories("com.toady.web.app.*.repository")
@EnableTransactionManagement
@ComponentScan
public class JpaConfig {

    @Autowired
    private DataSource dataSource;
    
    
    @Bean
    public JpaTransactionManager transactionManager(){
        return new JpaTransactionManager();
    }

    // LocalContainerEntityManagerFactoryBean : JPA를 스프링 컨테이너에서 사용할 수 있도록 스프링프레임워크가 제공하는 기능
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPackagesToScan("com.toady.web.app.*.entity"); // @Entity 탐색위치
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());   // JPA 구현체로는 하이버네이트를 사용

        // 상세설정
        Properties jpaProperties = new Properties();
        jpaProperties.put(AvailableSettings.SHOW_SQL, false);    // SQL 보기
        jpaProperties.put(AvailableSettings.FORMAT_SQL, true);  // SQL 정렬해서 보기
        jpaProperties.put(AvailableSettings.USE_SQL_COMMENTS, false);    // SQL 코멘트 보기
        jpaProperties.put(AvailableSettings.HBM2DDL_AUTO, "none");    // DDL 자동생성  (none, update)
        jpaProperties.put(AvailableSettings.DIALECT, "org.hibernate.dialect.MariaDB103Dialect");    // 방언 설정
        jpaProperties.put(AvailableSettings.USE_NEW_ID_GENERATOR_MAPPINGS, true);   // 새 버젼의 ID생성 옵션
        //jpaProperties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy"); //5미만 버전까지만 사용가능한 옵션.
        jpaProperties.put("hibernate.physical_naming_strategy", "com.toady.web.config.PhysicalNamingStrategyImpl" ); //카멜케이스를 언더스코어 명으로 변경.

        factoryBean.setJpaProperties(jpaProperties);

        return factoryBean;

    }

}
