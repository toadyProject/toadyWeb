package com.toady.web.config;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.MySQLTemplates;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.spring.SpringConnectionProvider;
import com.querydsl.sql.spring.SpringExceptionTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

/**
 * packageName    : com.toady.web.config
 * fileName       : QueryDslConfiguration
 * author         : youn
 * date           : 2022/05/19
 * description    : QueryDsl 설정.
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */
@org.springframework.context.annotation.Configuration
public class QueryDslConfiguration {

    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    private DataSource dataSource;

    @Bean
    public Configuration configuration() {
         SQLTemplates templates = MySQLTemplates.builder().printSchema().build();
         Configuration configuration = new Configuration(templates);
         configuration.setExceptionTranslator(new SpringExceptionTranslator());
         return configuration;
    }
    
    @Bean
    public SQLQueryFactory queryFactory() {
         //Provider<Connection> provider = new SpringConnectionProvider(dataSource);
         return new SQLQueryFactory(configuration(), new SpringConnectionProvider(dataSource));
    }
    
    @Bean
    public JPAQueryFactory jpaQueryFactory() {
        return new JPAQueryFactory(entityManager);
    }
    
}
