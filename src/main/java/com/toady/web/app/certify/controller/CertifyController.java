package com.toady.web.app.certify.controller;

import com.toady.web.app.certify.dto.CertifyDTO;
import com.toady.web.app.certify.service.CertifyService;
import com.toady.web.core.constants.ResultCode;
import com.toady.web.core.response.ResData;
import com.toady.web.core.util.CmnUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

/**
 * packageName    : com.toady.web.app.certify.controller
 * fileName       : CertifyController
 * author         : youn
 * date           : 2022/06/11
 * description    : 인증 컨트롤러
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/11        youn       최초 생성
 */
@Log4j2
@RestController
@RequestMapping("/webApi/certify")
public class CertifyController {

    @Autowired
    private CertifyService certifyService;

    /**
     * 인증
     * @param map
     * @return 결과.
     */
    @PostMapping("/certifyCheck")
    public ResponseEntity<ResData> certifyCheck(@RequestBody Map<String, Object> map){

        CertifyDTO.CheckParam certifyParam = new CertifyDTO().new CheckParam();
        certifyParam.setCertifyCode(map.getOrDefault("certifyCode", "").toString());
        certifyParam.setUserEmail(map.getOrDefault("userEmail", "").toString());

        return certifyService.certifyCheck(certifyParam);
    }


    /**
     * 비밀번호 찾기 인증번호 요청.
     * @return 결과.
     */
    @PostMapping("/findPasswordRequestCertify")
    public ResponseEntity<ResData> findPasswordRequestCertify(@RequestBody CertifyDTO.UserInfo userInfo){
        return certifyService.findPasswordRequestCertify(userInfo);
    }

    /**
     * 비밀번호 찾기 인증번호 확인 및 비밀번호 변경.
     * @return 결과.
     */
    @PostMapping("/certifyConfrimAndPasswordChange")
    public ResponseEntity<ResData> certifyConfrimAndPasswordChange(@Valid @RequestBody CertifyDTO.PasswordChange param, BindingResult result){
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResData.of(ResultCode.VALID_ERROR.getCode(), CmnUtils.getErrMsg(result, '\n')));
        }

        return certifyService.certifyConfrimAndPasswordChange(param);
    }
}
