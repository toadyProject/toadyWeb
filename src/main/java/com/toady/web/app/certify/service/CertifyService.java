package com.toady.web.app.certify.service;

import com.toady.web.app.certify.dto.CertifyDTO;
import com.toady.web.app.certify.entity.CertifyEntity;
import com.toady.web.app.certify.repository.CertifyRepository;
import com.toady.web.app.user.entity.UserEntity;
import com.toady.web.app.user.repository.UserRepository;
import com.toady.web.core.constants.ResultCode;
import com.toady.web.core.response.ResData;
import com.toady.web.core.util.StrUtils;
import com.toady.web.core.util.email.EmailUtilService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * packageName    : com.toady.web.app.certify.service
 * fileName       : CertifyService
 * author         : youn
 * date           : 2022/06/11
 * description    : 인증 서비스
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/11        youn       최초 생성
 */
@Log4j2
@Service
public class CertifyService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CertifyRepository certifyRepository;

    @Autowired
    private EmailUtilService emailUtilService;

    @Value("${toady.web.DOMAIN}")
    private String DOMAIN;

    @Autowired
    private PasswordEncoder passwordEncoder;


    /**
     * 인증정보 등록
     */
    public void saveCertify(CertifyDTO.Save param){

        try {
            String certifyCode = StrUtils.createCode();

            StringBuilder sendMessage = new StringBuilder();
            String url = DOMAIN+"/certify?userEmail="+param.getUserEmail()+"&certifyCode="+certifyCode;

            sendMessage
                .append("<html>")
                .append("<body>")
                .append("<table style='min-width: 350px; margin:0 auto; text-align:center; border:1px solid #000; margin-top:30px;'>")
                .append("    <tbody>")
                .append("    <tr>")
                .append("        <td style='padding-top:30px;'>")
                .append("            <img src='https://www.toady.co.kr/cont/images/logo/logo.png' style='height:150px; margin-top:30px; margin-bottom:3px; '></img>")
                .append("            <h2>두껍아</h2>")
                .append("            <br/>")
                .append("            <hr style='width:70%; border-color:#000;'>")
                .append("        </td>")
                .append("    </tr>")
                .append("    <tr>")
                .append("        <td><h2 style='font-weight:400;'>안녕하세요 두껍아 입니다.<br/>"+param.getUserNicName()+"님의 회원가입을 완료 하려면 아래 URL을 클릭 해 주세요.</h2></td>")
                .append("    </tr>")
                .append("    <tr>")
                .append("        <td>")
                .append("            <p style='width:60%; margin:0 auto; background-color:#efefef; padding:30px;'>")
                .append("                    URL : <a  style='cursor:point' href='"+url+"'>"+url+"</a>")
                .append("            </p>")
                .append("        </td>")
                .append("    </tr>")
                .append("    <tr>")
                .append("        <td>")
                .append("            <hr style='width:70%; border-color:#000;'>")
                .append("            <p style='font-size:8pt; color:#a2a2a2; margin-bottom:30px;'><br><br>")
                .append("                    For questions about our services contact us by email("+DOMAIN+").")
                .append("            </p></td>")
                .append("    </tr>")
                .append("    <tr>")
                .append("        <td>")
                .append("            <p style='background-color:#efefef; min-height:10px; color:#444; padding:20px; margin:0;'>")
                .append("                <strong>Email.</strong> dukkeoba@gmail.com")
                .append("                &nbsp;&nbsp;&nbsp;<strong>Homepage.</strong> "+DOMAIN)
                .append("            </p></td>")
                .append("    </tr>")
                .append("    </tbody>")
                .append("</table>")
                .append("</body>")
                .append("</html>");


            CertifyEntity certifyParam = CertifyEntity.builder()
                    .certifyType(param.getCertifyType())
                    .certifySendType(param.getSendType())
                    .certifyCode(certifyCode) //인증 값.
                    .certifyMessage(sendMessage.toString())
                    .certifyYn("N")
                    .createUserSeq(param.getUserSeq())
                    .createDt(LocalDateTime.now())
                    .build();

            //인증값을 저장한다.
            certifyRepository.save(certifyParam);

            //String toAddress, String subject, String body
            emailUtilService.sendEmail(param.getUserEmail(), "[Toady] 두껍아 회원가입 인증 요청", sendMessage.toString());

        } catch (Exception e) {
            log.info("인증 정보 등록 에러.");
            e.printStackTrace();
        }

    }


    /**
     * 이메일 인증 확인
     */
    public ResponseEntity<ResData> certifyCheck(CertifyDTO.CheckParam param){

        try {

            UserEntity userEntity = userRepository.findByUserEmail(param.getUserEmail());

            if(userEntity == null){
                return ResponseEntity.ok().body(ResData.of(ResultCode.EMPTY_USER_ERROR.getCode(), ResultCode.EMPTY_USER_ERROR.getMessage())); //사용자 정보가 없습니다.
            }

            CertifyEntity certifyEntity = certifyRepository.findByCertifyTypeAndCertifySendTypeAndCertifyCodeAndCreateUserSeq("ACCOUNT", "EMAIL", param.getCertifyCode(), userEntity.getUserSeq());

            if(certifyEntity == null){
                return ResponseEntity.ok().body(ResData.of(ResultCode.EMPTY_CERTIFY_REQUEST_ERROR.getCode(), ResultCode.EMPTY_CERTIFY_REQUEST_ERROR.getMessage())); //인증 내역이 존재하지 않습니다.
            }

            if("Y".equals(certifyEntity.getCertifyYn())){
                return ResponseEntity.ok().body(ResData.of(ResultCode.ALREADY_CERTIFY_ERROR.getCode(), ResultCode.ALREADY_CERTIFY_ERROR.getMessage())); //이미 인증됐습니다.
            }

            certifyEntity.setCertifyYn("Y");
            certifyEntity.setUpdateUserSeq(userEntity.getUserSeq());
            certifyEntity.setUpdateDt(LocalDateTime.now());

            //인증값을 수정한다.
            certifyRepository.save(certifyEntity);

            userEntity.setEmailCertifyYn("Y");
            userEntity.setUpdateDt(LocalDateTime.now());
            userRepository.save(userEntity);




        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }


        return ResponseEntity.ok().body(ResData.of(ResultCode.CERTIFY_SUCCESS.getCode(), ResultCode.CERTIFY_SUCCESS.getMessage()));

    }

    /**
     * 비밀번호 찾기 인증번호 요청.
     * @param param
     * @return
     */
    public ResponseEntity<ResData> findPasswordRequestCertify(CertifyDTO.UserInfo param){
        Map<String, String> resultMap = new HashMap<>();
        UserEntity userEntity = userRepository.findByUserEmail(param.getUserEmail());

        if(userEntity == null){
            return ResponseEntity.ok().body(ResData.of(ResultCode.EMPTY_USER_ERROR.getCode(), ResultCode.EMPTY_USER_ERROR.getMessage())); //사용자 정보가 없습니다.
        }

        CertifyEntity certifyEntity = certifyRepository.findTop1ByCertifyTypeAndCertifySendTypeAndCreateUserSeqOrderByCreateDtDesc("FIND_PW", "EMAIL", userEntity.getUserSeq());

        if(certifyEntity != null){
            // 로컬 컴퓨터의 현재 시간 정보를 저장한 LocalDate 객체를 리턴.
            LocalDateTime currentTime = LocalDateTime.now();
            long time = ChronoUnit.MINUTES.between(certifyEntity.getCreateDt(), currentTime);

            //System.out.println("time : " + time+((int)time < 2));

            if((int)time < 4){
                return ResponseEntity.ok().body(ResData.of(ResultCode.RE_CERTIFY_REQUEST_ERROR.getCode(), ResultCode.RE_CERTIFY_REQUEST_ERROR.getMessage()));
            }
        }

        try {
            String certifyCode = StrUtils.createCode();
            certifyCode = certifyCode.replaceAll("-","");

            StringBuilder sendMessage = new StringBuilder();
            // String url = DOMAIN+"/certify?userEmail="+param.getUserEmail()+"&certifyCode="+certifyCode;

            sendMessage
                    .append("<html>")
                    .append("<body>")
                    .append("<table style='min-width: 350px; margin:0 auto; text-align:center; border:1px solid #000; margin-top:30px;'>")
                    .append("    <tbody>")
                    .append("    <tr>")
                    .append("        <td style='padding-top:30px;'>")
                    .append("            <img src='https://www.toady.co.kr/cont/images/logo/logo.png' style='height:150px; margin-top:30px; margin-bottom:3px; '></img>")
                    .append("            <h2>두껍아</h2>")
                    .append("            <br/>")
                    .append("            <hr style='width:70%; border-color:#000;'>")
                    .append("        </td>")
                    .append("    </tr>")
                    .append("    <tr>")
                    .append("        <td><h2 style='font-weight:400;'>안녕하세요 두껍아 입니다.<br/>"+userEntity.getUserNicName()+"님의 비밀번호 변경을 원하시면 아래의 코드를 복사 후 사이트에 입력 해 주세요.<br/>인증 번호는 10분간 유효합니다.</h2></td>")
                    .append("    </tr>")
                    .append("    <tr>")
                    .append("        <td>")
                    .append("            <p style='width:60%; margin:0 auto; background-color:#efefef; padding:30px;'>")
                    .append("                    인증코드 : "+certifyCode)
                    .append("            </p>")
                    .append("        </td>")
                    .append("    </tr>")
                    .append("    <tr>")
                    .append("        <td>")
                    .append("            <hr style='width:70%; border-color:#000;'>")
                    .append("            <p style='font-size:8pt; color:#a2a2a2; margin-bottom:30px;'><br><br>")
                    .append("                    For questions about our services contact us by email("+DOMAIN+").")
                    .append("            </p></td>")
                    .append("    </tr>")
                    .append("    <tr>")
                    .append("        <td>")
                    .append("            <p style='background-color:#efefef; min-height:10px; color:#444; padding:20px; margin:0;'>")
                    .append("                <strong>Email.</strong> dukkeoba@gmail.com")
                    .append("                &nbsp;&nbsp;&nbsp;<strong>Homepage.</strong> "+DOMAIN)
                    .append("            </p></td>")
                    .append("    </tr>")
                    .append("    </tbody>")
                    .append("</table>")
                    .append("</body>")
                    .append("</html>");


            CertifyEntity certifyParam = CertifyEntity.builder()
                    .certifyType("FIND_PW")
                    .certifySendType("EMAIL")
                    .certifyCode(certifyCode) //인증 값.
                    .certifyMessage(sendMessage.toString())
                    .certifyYn("N")
                    .createUserSeq(userEntity.getUserSeq())
                    .createDt(LocalDateTime.now())
                    .build();

            //인증값을 저장한다.
            certifyRepository.save(certifyParam);

            //String toAddress, String subject, String body
            emailUtilService.sendEmail(param.getUserEmail(), "[Toady] 두껍아 비밀번호 찾기 인증번호 요청", sendMessage.toString());

            resultMap.put("userEmail", userEntity.getUserEmail());

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }


        return ResponseEntity.ok().body(ResData.of(resultMap,ResultCode.SUCCESS.getCode(), "["+userEntity.getUserEmail()+"]로 인증번호를 전송했습니다.<br/>메일로 전송된 인증번호를 입력해 주세요."));
    }


    /**
     * 비밀번호 찾기 인증번호 확인 및 비밀번호 변경.
     * @param param
     */
    public ResponseEntity<ResData> certifyConfrimAndPasswordChange(CertifyDTO.PasswordChange param){



        try {

            UserEntity userEntity = userRepository.findByUserEmail(param.getUserEmail());
            if(userEntity == null){
                //사용자 정보가 없습니다.
                return ResponseEntity.ok().body(ResData.of(ResultCode.EMPTY_USER_ERROR.getCode(), ResultCode.EMPTY_USER_ERROR.getMessage()));
            }

            CertifyEntity certifyEntity = certifyRepository.findByCertifyTypeAndCertifySendTypeAndCertifyCodeAndCreateUserSeq("FIND_PW", "EMAIL", param.getCertifyCode(), userEntity.getUserSeq());

            if(certifyEntity == null){
                //인증 내역이 존재하지 않습니다.
                return ResponseEntity.ok().body(ResData.of(ResultCode.EMPTY_CERTIFY_REQUEST_ERROR.getCode(), ResultCode.EMPTY_CERTIFY_REQUEST_ERROR.getMessage()));
            }

            if("Y".equals(certifyEntity.getCertifyYn())){
                //이미 인증됐습니다.
                return ResponseEntity.ok().body(ResData.of(ResultCode.ALREADY_CERTIFY_ERROR.getCode(), ResultCode.ALREADY_CERTIFY_ERROR.getMessage()));
            }

            // 로컬 컴퓨터의 현재 시간 정보를 저장한 LocalDate 객체를 리턴.
            LocalDateTime currentTime = LocalDateTime.now();
            long time = ChronoUnit.MINUTES.between(certifyEntity.getCreateDt(), currentTime);

            if((int)time > 10){
                return ResponseEntity.ok().body(ResData.of(ResultCode.EXPIRATION_TIME_EXCEEDED_ERROR.getCode(), ResultCode.EXPIRATION_TIME_EXCEEDED_ERROR.getMessage()));
            }

            if(!param.getUserPassword().equals(param.getUserPasswordConfirm())){
                return ResponseEntity.ok().body(ResData.of(ResultCode.PASSWORD_MISMATCH_ERROR.getCode(), ResultCode.PASSWORD_MISMATCH_ERROR.getMessage()));
            }

            //Pattern passPattern = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"); //6자 영문+특문+숫자
            Pattern passPattern = Pattern.compile("^(?=.*[A-Za-z])((?=.*\\d)|(?=.*\\W)).{6,}$");
            Matcher passMatcher = passPattern.matcher(param.getUserPassword());

            if (!passMatcher.find()) {
                return ResponseEntity.ok().body(ResData.of(ResultCode.PASSWORD_PATTERN_VALID1_ERROR.getCode(), ResultCode.PASSWORD_PATTERN_VALID1_ERROR.getMessage()));
            }

            //비밀번호 변경.
            userEntity.setUserPassword(passwordEncoder.encode(param.getUserPassword()));
            userEntity.setUpdateDt(LocalDateTime.now());
            userRepository.save(userEntity);

            certifyEntity.setCertifyYn("Y");
            certifyEntity.setUpdateUserSeq(userEntity.getUserSeq());
            certifyEntity.setUpdateDt(LocalDateTime.now());

            //인증값을 수정한다.
            certifyRepository.save(certifyEntity);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }

        return ResponseEntity.ok().body(ResData.of(ResultCode.SUCCESS.getCode(), "성공적으로 비밀번호가 변경 되었습니다."));
    }
}
