package com.toady.web.app.user.service;

import com.toady.web.app.certify.dto.CertifyDTO;
import com.toady.web.app.certify.service.CertifyService;
import com.toady.web.app.user.dto.UserDTO;
import com.toady.web.app.user.entity.UserEntity;
import com.toady.web.app.user.repository.UserRepository;
import com.toady.web.core.constants.ResultCode;
import com.toady.web.core.response.ResData;
import com.toady.web.core.security.jwt.JwtProvider;
import com.toady.web.core.security.service.CustomUserDetails;
import com.toady.web.core.security.service.SimpleUserDetailsService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * packageName    : com.toady.web.app.user.service
 * fileName       : UserService
 * author         : youn
 * date           : 2022/06/09
 * description    : 유저 서비스
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/09        youn       최초 생성
 */
@Log4j2
@Service
public class UserService {

    @Autowired
    private SimpleUserDetailsService simpleUserDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CertifyService certifyService;

    /**
     * 유저 정보 가져오기
     */
    public Map<String, Object> getUser(UserDTO.Login param) throws Exception {

        //UserDetails userInfo = simpleUserDetailsService.loadUserByUsername(param.getUserEmail()); //유저정보가 없을경우 UsernameNotFoundException 발생
        CustomUserDetails userInfo = (CustomUserDetails) simpleUserDetailsService.loadUserByUsername(param.getUserEmail().toLowerCase()); //유저정보가 없을경우 UsernameNotFoundException 발생

        if (passwordEncoder.matches(param.getUserPassword() , userInfo.getPassword()) == false) { //비밀번호 체크
            throw new BadCredentialsException(ResultCode.BAD_CREDENTIALS_ERROR.getMessage());
        }


        //토큰 생성.
        String accessToken = generatorToken(userInfo);

        //결과로 넘기기는 Map 생성.
        Map<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("accessToken", accessToken);

        //System.out.println("accessToken : " + accessToken);

        return rtnMap;
    }

    /**
     * 토큰 생성
     */
    public String generatorToken(CustomUserDetails details) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(details, details.getPassword(), details.getAuthorities());
        return jwtProvider.generatorToken(authentication);
    }


    /**
     * 회원가입
     * @param param
     * @return
     */
    public ResponseEntity<ResData> regUser(UserDTO.RegUser param){

        try {

            //email 소문자로 변경.
            param.setUserEmail(param.getUserEmail().toLowerCase());

            if(userRepository.findByUserEmail(param.getUserEmail() ) != null){
                return ResponseEntity.ok().body(ResData.of(ResultCode.ALREADY_EMAIL_ERROR.getCode(), ResultCode.ALREADY_EMAIL_ERROR.getMessage()));
            }

            if(userRepository.findByUserNicName(param.getUserNicName())  != null){
                return ResponseEntity.ok().body(ResData.of(ResultCode.ALREADY_NIC_NAME.getCode(), ResultCode.ALREADY_NIC_NAME.getMessage()));
            }

            if(!param.getUserPassword().equals(param.getUserPasswordConfirm())){
                return ResponseEntity.ok().body(ResData.of(ResultCode.PASSWORD_MISMATCH_ERROR.getCode(), ResultCode.PASSWORD_MISMATCH_ERROR.getMessage()));
            }

            //Pattern passPattern = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"); //6자 영문+특문+숫자
            Pattern passPattern = Pattern.compile("^(?=.*[A-Za-z])((?=.*\\d)|(?=.*\\W)).{6,}$");
            Matcher passMatcher = passPattern.matcher(param.getUserPassword());

            if (!passMatcher.find()) {
                return ResponseEntity.ok().body(ResData.of(ResultCode.PASSWORD_PATTERN_VALID1_ERROR.getCode(), ResultCode.PASSWORD_PATTERN_VALID1_ERROR.getMessage()));
            }

            UserEntity userParam = UserEntity.builder()
                    .userEmail(param.getUserEmail())
                    .userPassword(passwordEncoder.encode(param.getUserPassword()))
                    .userNicName(param.getUserNicName())
                    .emailCertifyYn("N")
                    .lockYn("N")
                    .withdrawalYn("N")
                    .createDt(LocalDateTime.now())
                    .build();

            userRepository.save(userParam);


            CertifyDTO.Save certifyParam = new CertifyDTO().new Save();


            certifyParam.setUserEmail(userParam.getUserEmail());
            certifyParam.setUserNicName(userParam.getUserNicName());
            certifyParam.setCertifyType("ACCOUNT");
            certifyParam.setSendType("EMAIL");
            certifyParam.setUserSeq(userParam.getUserSeq());

            //type email userSeq
            certifyService.saveCertify(certifyParam);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }

        return ResponseEntity.ok().body(ResData.of(ResultCode.SUCCESS.getCode(), "성공적으로 회원가입 되었습니다. <br/>["+param.getUserEmail()+"]로 인증 URL을 전송했습니다.<br/> 인증 후 로그인 가능하며, <br/>24시간동안 인증이 완료 되지 않으면 회원 가입이 취소됩니다."));
    }
}
