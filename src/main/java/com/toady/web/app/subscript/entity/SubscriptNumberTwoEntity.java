package com.toady.web.app.subscript.entity;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptNumberTwoEntity
 * author         : youn
 * date           : 2022/05/19
 * description    : 공급금액, 2순위 청약금
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Comment;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(
        name="T_SUBSCRIPT_NUMBER_TWO",
        uniqueConstraints = {
                @UniqueConstraint(
                        name="uk_subscriptNumberTwo",
                        columnNames = {"SUBSCRIPT_SEQ", "RCEPT_SE", "TWO_HOUSING_TYPE"}
                )
        }
)
public class SubscriptNumberTwoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptNumberTwoSeq;

    @Column(name="SUBSCRIPT_SEQ", nullable = false, length = 20)
    private Long subscriptSeq;

    @Comment("01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대")
    @Column(name="RCEPT_SE", nullable = false, length = 2)
    private String rceptSe;

    @Comment("주택형")
    @Column(name="TWO_HOUSING_TYPE", nullable = false, length = 15)
    private String twoHousingType;

    @Comment("군")
    @Column(length = 25)
    private String gun;

    @Comment("청약신청금")
    @Column(length = 11)
    private String subscriptAmount;

    @Comment("공급금액(최고가 기준)")
    @Column(length = 11)
    private String twoSupplyAmount;

    @Comment("등록일시")
    private LocalDateTime createDt;

    @Comment("수정일시")
    private LocalDateTime updateDt;

}
