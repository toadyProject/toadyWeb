package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.entity.SubscriptEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * packageName    : com.toady.web.app.subscript.repository
 * fileName       : SubscriptRepository
 * author         : youn
 * date           : 2022/05/19
 * description    : 청약일정 및 정보 Repository
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */
public interface SubscriptRepository extends JpaRepository<SubscriptEntity,Long> , SubscriptRepositoryCustom {

    /**
     * 청약일정 및 정보 목록 조회.
     */
    public List<SubscriptEntity> findByInDateBetweenAndRceptSeIn(String startDate, String endDate, ArrayList rceptSeAry);

}
