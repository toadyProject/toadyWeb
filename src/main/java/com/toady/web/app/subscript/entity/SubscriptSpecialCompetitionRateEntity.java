package com.toady.web.app.subscript.entity;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptSpecialCompetitionRateEntity
 * author         : youn
 * date           : 2023/11/19
 * description    : 특별공급 경쟁률
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2023/11/19        youn       최초 생성
 */

import lombok.*;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@AllArgsConstructor //모든 필드 값을 파라미터로 받는 생성자를 만듦
@NoArgsConstructor
@Getter
@Setter
@DynamicInsert
@Table(
        name = "T_SUBSCRIPT_SPECIAL_COMPETITION_RATE",
        uniqueConstraints = {
                @UniqueConstraint(
                        name="uk_subscriptSpecialCompetitionRate",
                        columnNames={"SUBSCRIPT_SEQ", "HOUSING_TYPE", "RCEPT_SE", "LOCATION_TYPE"}
                )
        }

)
public class SubscriptSpecialCompetitionRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptSpecialCompetitionRateSeq;

    @Column(name="SUBSCRIPT_SEQ", nullable = false, length = 20)
    private Long subscriptSeq;

    @Comment("주택형")
    @Column(name = "HOUSING_TYPE", length = 25)
    private String housingType; //주택형

    @Comment("공급세대수")
    @Column(name = "SUPPLY_CNT", length = 50)
    private String supplyCnt; // 공급세대수

    @Comment("01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대")
    @Column(name = "RCEPT_SE", nullable = false, length = 2)
    private String rceptSe;  //01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대

    @Comment("지역 구분")
    @Column(name = "LOCATION_TYPE", length = 20)
    private String locationType; // 지역 구분

    @Comment("다자녀가구")
    @Column(name = "MULTI_FAMILY_CNT", length = 11)
    private String multiFamilyCnt; // 다자녀가구

    @Comment("신혼부부")
    @Column(name = "HONEYMOON_CNT", length = 11)
    private String honeymoonCnt; // 신혼부부

    @Comment("생애최초")
    @Column(name = "FIRST_TIME_IN_LIFE_CNT", length = 11)
    private String firstTimeInLifeCnt; // 생애최초

    @Comment("청년")
    @Column(name = "YOUTH_CNT", length = 11)
    private String youthCnt; // 청년

    @Comment("노부모 부양")
    @Column(name = "OLD_PARENTS_CNT", length = 11)
    private String oldParentsCnt; // 노부모 부양

    @Comment("기관추천")
    @Column(name = "RECOMMENDATION_CNT", length = 11)
    private String recommendationCnt; // 기관추천

    @Comment("이전기관")
    @Column(name = "FORMER_INSTITUTION_CNT", length = 11)
    private String formerInstitutionCnt; // 이전기관

    @Comment("청약 결과")
    @Column(name = "LOTTERY_RESULT", length = 11)
    private String lotteryResult; // 청약 결과

    @Comment("등록일시")
    private LocalDateTime createDt;

    @Comment("수정일시")
    private LocalDateTime updateDt;
}
