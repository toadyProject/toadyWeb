package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.entity.SubscriptAccessLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * packageName    : com.toady.web.app.subscript.repository
 * fileName       : SubscriptAccessLogRepository
 * author         : youn
 * date           : 2022/07/16
 * description    : 청약 상세 정보 조회 로그 Repository
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/07/16        youn       최초 생성
 */
public interface SubscriptAccessLogRepository extends JpaRepository<SubscriptAccessLogEntity,Long> {



}
