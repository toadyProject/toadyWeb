package com.toady.web.app.subscript.entity;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptCommentEntity
 * author         : youn
 * date           : 2022/06/06
 * description    : 청약 댓글
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/06        youn       최초 생성
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "T_SUBSCRIPT_COMMENT")
public class SubscriptCommentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptCommentSeq;

    @Comment("청약 아파트 고유 번호")
    @Column(length = 10)
    private String houseManageNo;

    @Column(nullable = false, length = 20)
    private Long subscriptSeq;

    @Comment("내용")
    private String content;

    @Comment("추천")
    @ColumnDefault("0")
    private Integer recommend;

    @Comment("반대")
    @ColumnDefault("0")
    private Integer oppose;

    @Comment("부모 Seq")
    @Column(length = 20)
    private Long parentSubscriptCommentSeq;

    @Comment("삭제 유무")
    @Column(nullable = false, length = 1)
    @ColumnDefault("'N'")
    private String deleteYn;

    @Comment("작성자")
    @Column(nullable = false, length = 20)
    private Long createUserSeq;

    @Comment("등록일시")
    @CreationTimestamp
    private LocalDateTime createDt;

    @Comment("수정자")
    private Long updateUserSeq;

    @Comment("수정일시")
    private LocalDateTime updateDt;

}