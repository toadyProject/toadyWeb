package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.entity.SubscriptSupplyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * packageName    : com.toady.web.app.subscript.repository
 * fileName       : SubscriptSupplyRepository
 * author         : youn
 * date           : 2022/05/19
 * description    : 입주자모집공고 공급대상 정보 Repository
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */
public interface SubscriptSupplyRepository extends JpaRepository<SubscriptSupplyEntity,Long> {

    /**
     * 입주자모집공고 공급대상 정보 조회.
     */
    public List<SubscriptSupplyEntity> findBySubscriptSeqOrderBySubscriptSupplySeqAsc (Long subscriptSeq);

}
