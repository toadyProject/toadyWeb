package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.entity.SubscriptNumberTwoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * packageName    : com.toady.web.app.subscript.repository
 * fileName       : SubscriptNumberTwoRepository
 * author         : youn
 * date           : 2022/05/19
 * description    : 공급금액, 2순위 청약금 정보 Repository
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */
public interface SubscriptNumberTwoRepository extends JpaRepository<SubscriptNumberTwoEntity,Long> {


    /**
     * 공급금액, 2순위 청약금 정보 조회.
     */
    public List<SubscriptNumberTwoEntity> findBySubscriptSeqOrderBySubscriptNumberTwoSeqAsc (Long subscriptSeq);

}
