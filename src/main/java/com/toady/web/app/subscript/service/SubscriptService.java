package com.toady.web.app.subscript.service;

import com.querydsl.core.Tuple;
import com.toady.web.app.subscript.dto.SubscriptDTO;
import com.toady.web.app.subscript.dto.SubscriptListDto;
import com.toady.web.app.subscript.entity.QSubscriptCommentEntity;
import com.toady.web.app.subscript.entity.SubscriptAccessLogEntity;
import com.toady.web.app.subscript.entity.SubscriptCommentEntity;
import com.toady.web.app.subscript.entity.SubscriptEntity;
import com.toady.web.app.subscript.repository.*;
import com.toady.web.app.user.entity.QUserEntity;
import com.toady.web.core.constants.ResultCode;
import com.toady.web.core.response.ResData;
import com.toady.web.core.util.CmnUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

/**
 * packageName    : com.toady.web.app.subscript.service
 * fileName       : SubscriptService
 * author         : youn
 * date           : 2022/05/23
 * description    : 청약 일정을 조회한다.
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/23        youn       최초 생성
 */
@Log4j2
@Service
@RequiredArgsConstructor // final이 붙거나 @NotNull 이 붙은 필드의 생성자를 자동 생성
public class SubscriptService {


    //청약일정 및 정보
    private final SubscriptRepository subscriptRepository;

    //공급금액, 2순위 청약금
    private final SubscriptNumberTwoRepository subscriptNumberTwoRepository;

    //입주자모집공고 특별공급 공급대상
    private final SubscriptSpecialRepository subscriptSpecialRepository;

    //입주자모집공고 공급대상
    private final SubscriptSupplyRepository subscriptSupplyRepository;

    //청약일정 커멘트
    private final SubscriptCommentRepository subscriptCommentRepository;

    //청약 상세 정보 조회 로그 Repository
    private final SubscriptAccessLogRepository subscriptAccessLogRepository;

    //특별공급 경쟁률 Repository
    private final SubscriptSpecialCompetitionRateRepository subscriptSpecialCompetitionRateRepository;

    private SubscriptRepositoryCustom subscriptRepositoryCustom;

    /**
     * 청약 일정 목록 조회.
     */
    public List<SubscriptListDto> getSubscriptList(SubscriptDTO.Search param) {

        ArrayList<String> rceptSeAry = new ArrayList();

        if("Y".equals(param.getRceptSe01())){
            rceptSeAry.add("01");
        }
        if("Y".equals(param.getRceptSe02())){
            rceptSeAry.add("02");
        }
        if("Y".equals(param.getRceptSe03())){
            rceptSeAry.add("03");
        }
        if("Y".equals(param.getRceptSe04())){
            rceptSeAry.add("04");
        }
        if("Y".equals(param.getRceptSe05())){
            rceptSeAry.add("05");
        }
        if("Y".equals(param.getRceptSe06())){
            rceptSeAry.add("06");
        }
        if("Y".equals(param.getRceptSe07())){
            rceptSeAry.add("07");
        }
        if("Y".equals(param.getRceptSe08())){
            rceptSeAry.add("08");
        }
        if("Y".equals(param.getRceptSe09())){
            rceptSeAry.add("09");
        }
        if("Y".equals(param.getRceptSe10())){
            rceptSeAry.add("10");
        }


        param.setStartStr(param.getStartStr());
        param.setEndStr(param.getEndStr());

        //return subscriptRepository.getSubscriptList() .findByInDateBetweenAndRceptSeIn(param.getStartStr()+"01", param.getEndStr()+"31", rceptSeAry);
        return subscriptRepository.getSubscriptList(param, rceptSeAry);
    }

    /**
     * 청약 정보 상세 조회.
     */
    public Map<String, Object> getSubscriptDetail(SubscriptDTO.Detail param, HttpServletRequest request){

        Map<String, Object> resultMap = new HashMap<String, Object>();
        Optional<SubscriptEntity> subscriptDetail = subscriptRepository.findById(param.getSubscriptSeq());
        resultMap.put("subscriptDetail", subscriptRepository.findById(param.getSubscriptSeq()));

        //댓글 갯수를 조회한다.
        Integer subscriptCommentCnt = subscriptCommentRepository.countByHouseManageNoAndDeleteYn(subscriptDetail.get().getHouseManageNo(), "N");
        resultMap.put("subscriptCommentCnt", subscriptCommentCnt);

        resultMap.put("subscriptNumberTwo", subscriptNumberTwoRepository.findBySubscriptSeqOrderBySubscriptNumberTwoSeqAsc(param.getSubscriptSeq()));
        resultMap.put("subscriptSpecial", subscriptSpecialRepository.findBySubscriptSeqOrderBySubscriptSpecialSeqAsc(param.getSubscriptSeq()));
        resultMap.put("subscriptSupply", subscriptSupplyRepository.findBySubscriptSeqOrderBySubscriptSupplySeqAsc(param.getSubscriptSeq()));

        if("01".equals(subscriptDetail.get().getRceptSe())){ //특별공급.
            resultMap.put("subscriptSpecialCompetitionRateList", subscriptRepository.getSubscriptSpecialCompetitionRateList(param.getSubscriptSeq(), "01" ));
        }else if("02".equals(subscriptDetail.get().getRceptSe()) || "03".equals(subscriptDetail.get().getRceptSe())){ //일반공급
            resultMap.put("subscriptCompetitionRateList", subscriptRepository.getSubscriptCompetitionRateList(subscriptDetail.get().getHouseManageNo() ));
            resultMap.put("subscriptCompetitionRateScoreList", subscriptRepository.getSubscriptCompetitionRateScoreList(subscriptDetail.get().getHouseManageNo()));
        }

        if(subscriptDetail != null){
            try {
                CmnUtils cmnUtils = new CmnUtils();
                // System.out.println("test : " + cmnUtils.getUserIP(request));
                SubscriptAccessLogEntity subscriptAccessLogEntity = new SubscriptAccessLogEntity();
                subscriptAccessLogEntity.setSubscriptSeq(subscriptDetail.get().getSubscriptSeq());
                subscriptAccessLogEntity.setHouseManageNo(subscriptDetail.get().getHouseManageNo());
                subscriptAccessLogEntity.setAddressIp(cmnUtils.getUserIP(request));
                subscriptAccessLogEntity.setUserSeq(param.getUserSeq());
                subscriptAccessLogEntity.setCreateDt(LocalDateTime.now());

                subscriptAccessLogRepository.save(subscriptAccessLogEntity);
                log.info("SUCCESS : 상세 조회 로그 저장 성공.");
            } catch (Exception e ){
                log.info("FAIL : 상세 조회 로그 저장 실패.");
                e.printStackTrace();
            }
        }

        return resultMap;

    }


    /**
     * 청약 코멘트 목록 조회.
     * @param param
     * @return
     */
    public Map<String, Object> getSubscriptComment(SubscriptDTO.GetComment param) throws Exception {

        Map<String, Object> resultMap = new HashMap<String, Object>();

        List<Tuple> subscriptCommentList = null;
        subscriptCommentList = subscriptCommentRepository.getSubscriptCommentList(param);

        QSubscriptCommentEntity subscriptComment = QSubscriptCommentEntity.subscriptCommentEntity;
        QUserEntity userEntity = QUserEntity.userEntity;


        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();

        for (Tuple tuple : subscriptCommentList) {
            map = new HashMap<>();
            map.put("subscriptCommentSeq", tuple.get(subscriptComment.subscriptCommentSeq ));
            map.put("subscriptSeq", tuple.get(subscriptComment.subscriptSeq ));
            map.put("houseManageNo", tuple.get(subscriptComment.houseManageNo ));
            map.put("content", tuple.get(subscriptComment.content ));
            map.put("recommend", tuple.get(subscriptComment.recommend ));
            map.put("oppose", tuple.get(subscriptComment.oppose ));
            map.put("parentSubscriptCommentSeq", tuple.get(subscriptComment.parentSubscriptCommentSeq ));
            map.put("deleteYn", tuple.get(subscriptComment.deleteYn ));
            map.put("createUserSeq", tuple.get(subscriptComment.createUserSeq ));
            map.put("createDt", tuple.get(subscriptComment.createDt ));
            map.put("updateUserSeq", tuple.get(subscriptComment.updateUserSeq ));
            map.put("userNicName", tuple.get(userEntity.userNicName ));

            list.add(map);
        }

        resultMap.put("subscriptCommentList", list);

        return resultMap;
    }

    /**
     * 청약 코멘트 등록.
     * @param param
     */
    public ResponseEntity<ResData> saveSubscriptComment(SubscriptDTO.SaveComment param){

        try {

            SubscriptCommentEntity subscriptCommentParam = SubscriptCommentEntity.builder()
                    .houseManageNo(param.getHouseManageNo())
                    .subscriptSeq(param.getSubscriptSeq())
                    .content(param.getContent())
                    .recommend(0)
                    .oppose(0)
                    .parentSubscriptCommentSeq(param.getParentSubscriptCommentSeq())
                    .deleteYn("N")
                    .createUserSeq(param.getUserSeq())
                    .createDt(LocalDateTime.now())
                    .build();

            subscriptCommentRepository.save(subscriptCommentParam);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }

        return ResponseEntity.ok().body(ResData.of(ResultCode.SUCCESS.getCode(), "정상적으로 등록됐습니다."));
    }

    /**
     * 청약 코멘트 상세 조회.
     * @param param
     * @return
     */
    public ResponseEntity<ResData> getSubscriptCommentDetail(SubscriptDTO.GetCommentDetail param){

        Map<String, Object> resultMap = new HashMap<String, Object>();

        SubscriptCommentEntity subscriptCommentEntity = null;

        try {

            subscriptCommentEntity = subscriptCommentRepository.findBySubscriptCommentSeqAndDeleteYn(param.getSubscriptCommentSeq(), "N");
            resultMap.put("subscriptCommentDetail", subscriptCommentEntity);

        } catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }

        return ResponseEntity.ok().body(ResData.of(resultMap, ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage()));
    }


    /**
     * 청약 코멘트 수정.
     * @param param
     */
    public ResponseEntity<ResData> updateSubscriptComment(SubscriptDTO.UpdateComment param) {

        try {

            //기 등록된 커멘트를 조회한다.
            SubscriptCommentEntity subscriptCommentEntity = subscriptCommentRepository.findBySubscriptCommentSeqAndDeleteYn(param.getSubscriptCommentSeq(), "N");

            Integer childCnt = subscriptCommentRepository.countByParentSubscriptCommentSeqAndDeleteYn(param.getSubscriptCommentSeq(), "N");

            if (subscriptCommentEntity == null) {
                log.info("처리 할 코멘트가 존재하지 않습니다.");
                return ResponseEntity.ok().body(ResData.of(ResultCode.NO_COMMENTS_ERROR.getCode(), ResultCode.NO_COMMENTS_ERROR.getMessage()));
            }

            if(childCnt > 0){
                log.info("댓글이 달려 있어 수정/삭제 불가능 합니다.");
                return ResponseEntity.ok().body(ResData.of(ResultCode.COMMENTS_EXIST_ERROR.getCode(), ResultCode.COMMENTS_EXIST_ERROR.getMessage()));
            }

            if(subscriptCommentEntity.getCreateUserSeq().equals(param.getUserSeq())){

                if(!"Y".equals(param.getDeleteYn())){

                    if (param.getContent().length() < 1) {
                        log.info("댓글 내용을 작성해 주세요.");
                        return ResponseEntity.ok().body(ResData.of(ResultCode.COMMENT_CONTENT_ERROR.getCode(), ResultCode.COMMENT_CONTENT_ERROR.getMessage()));
                    }

                    subscriptCommentEntity.setContent(param.getContent());
                }else{
                    subscriptCommentEntity.setDeleteYn("Y");
                }

                subscriptCommentEntity.setUpdateDt(LocalDateTime.now());
                subscriptCommentEntity.setUpdateUserSeq(param.getUserSeq());

                subscriptCommentRepository.save(subscriptCommentEntity);

                log.info("댓글 처리 성공.");

            }else{
                log.info("댓글은 작성자만 처리 할 수 있습니다.");
                return ResponseEntity.ok().body(ResData.of(ResultCode.CURRENT_USER_CAN_MANAGE_COMMENT_ERROR.getCode(), ResultCode.CURRENT_USER_CAN_MANAGE_COMMENT_ERROR.getMessage()));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }

        return ResponseEntity.ok().body(ResData.of(ResultCode.SUCCESS.getCode(), "정상적으로 처리되었습니다."));
    }




}
