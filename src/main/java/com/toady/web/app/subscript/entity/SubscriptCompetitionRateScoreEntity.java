package com.toady.web.app.subscript.entity;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptCompetitionRateScoreEntity
 * author         : youn
 * date           : 2023/11/26
 * description    : 가점 점수
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2023/11/26        youn       최초 생성
 */

import lombok.*;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@AllArgsConstructor //모든 필드 값을 파라미터로 받는 생성자를 만듦
@NoArgsConstructor
@Getter
@Setter
@DynamicInsert
@Table(
        name = "T_SUBSCRIPT_COMPETITION_RATE_SCORE",
        uniqueConstraints = {
                @UniqueConstraint(
                        name="uk_subscriptCompetitionRateScore",
                        columnNames={"HOUSE_MANAGE_NO","HOUSING_TYPE","LOCATION_TYPE"}
                )
        }

)
public class SubscriptCompetitionRateScoreEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptCompetitionRateScoreSeq;

    @Comment("아파트 고유 번호")
    @Column(name="HOUSE_MANAGE_NO", nullable = false, length = 10)
    private String houseManageNo;

    @Comment("주택형")
    @Column(name = "HOUSING_TYPE", length = 25)
    private String housingType; //주택형

    @Comment("지역 구분")
    @Column(name = "LOCATION_TYPE", length = 20)
    private String locationType; // 지역 구분

    @Comment("최저 당첨 가점")
    @Column(name = "MIN_SCORE", length = 11)
    private String minScore;

    @Comment("최고 당첨 가점")
    @Column(name = "MAX_SCORE", length = 11)
    private String maxScore;

    @Comment("평균 당첨 가점")
    @Column(name = "AVG_SCORE", length = 11)
    private String avgScore;

    @Comment("등록일시")
    private LocalDateTime createDt;

    @Comment("수정일시")
    private LocalDateTime updateDt;
}
