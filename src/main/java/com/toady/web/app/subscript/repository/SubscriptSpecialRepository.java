package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.entity.SubscriptSpecialEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * packageName    : com.toady.web.app.subscript.repository
 * fileName       : SubscriptSpecialRepository
 * author         : youn
 * date           : 2022/05/19
 * description    : 입주자모집공고 특별공급 공급대상 정보 Repository
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */
public interface SubscriptSpecialRepository extends JpaRepository<SubscriptSpecialEntity,Long> {


    /**
     * 입주자모집공고 특별공급 공급대상 정보 조회.
     */
    public List<SubscriptSpecialEntity> findBySubscriptSeqOrderBySubscriptSpecialSeqAsc (Long subscriptSeq);

}
