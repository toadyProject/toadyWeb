package com.toady.web.app.subscript.entity;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptAccessLogEntity
 * author         : youn
 * date           : 2022/07/16
 * description    : 청약 상세 정보 조회 로그
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/07/16        youn       최초 생성
 */

import lombok.*;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@AllArgsConstructor //모든 필드 값을 파라미터로 받는 생성자를 만듦
@NoArgsConstructor
@Getter
@Setter
@DynamicInsert
@Table(name = "T_SUBSCRIPT_ACCESS_LOG")
public class SubscriptAccessLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptAccessLogSeq;


    private Long subscriptSeq;


    @Comment("청약 아파트 고유 번호")
    @Column(name="HOUSE_MANAGE_NO", length = 10)
    private String houseManageNo;

    @Comment("접속자 IP")
    @Column(nullable = true, length = 50)
    private String addressIp;

    @Comment("사용자 SEQ")
    @Column(nullable = true, length = 20)
    private Long userSeq;

    @Comment("최초등록일시")
    private LocalDateTime createDt;

}
