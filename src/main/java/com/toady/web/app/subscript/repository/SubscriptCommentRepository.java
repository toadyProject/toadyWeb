package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.entity.SubscriptCommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptCommentRepository
 * author         : youn
 * date           : 2022/06/08
 * description    : 청약 정보 커멘트 Repository
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/08        youn       최초 생성
 */
public interface SubscriptCommentRepository extends JpaRepository<SubscriptCommentEntity,Long>, SubscriptCommentRepositoryCustom {


    /**
     * 커멘트 내역 상세 조회.
     * @param subscriptCommentSeq
     * @param deleteYn
     */
    public SubscriptCommentEntity findBySubscriptCommentSeqAndDeleteYn(Long subscriptCommentSeq, String deleteYn);

    /**
     * 댓글 갯수 조회.
     * @param houseManageNo
     * @param deleteYn
     * @return
     */
    public Integer countByHouseManageNoAndDeleteYn(String houseManageNo, String deleteYn);

    /**
     * 자식이 존재하는지 조회한다.
     * @param houseManageNo
     * @param deleteYn
     * @return
     */
    public Integer countByParentSubscriptCommentSeqAndDeleteYn(Long parentSubscriptCommentSeq, String deleteYn);


}
