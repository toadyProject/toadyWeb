package com.toady.web.app.subscript.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.Subselect;
import org.hibernate.annotations.Synchronize;

import javax.persistence.*;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptCommentEntity
 * author         : youn
 * date           : 2022/06/06
 * description    : 청약 댓글
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/06        youn       최초 생성
 */
@Entity
@Getter
@Setter
@Subselect("select HOUSE_MANAGE_NO, count(1) as cnt from T_SUBSCRIPT_COMMENT where DELETE_YN='N' group by HOUSE_MANAGE_NO")
@Synchronize("T_SUBSCRIPT_COMMENT")
public class SubscriptCommentGroupByHouseManageNoEntity {

    @Id
    @Comment("청약 아파트 고유 번호")
    @Column(length = 10)
    private String houseManageNo;


    private Long cnt;

}