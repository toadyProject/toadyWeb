package com.toady.web.app.subscript.repository.impl;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.toady.web.app.subscript.dto.SubscriptDTO;
import com.toady.web.app.subscript.entity.QSubscriptCommentEntity;
import com.toady.web.app.subscript.entity.QSubscriptEntity;
import com.toady.web.app.subscript.entity.SubscriptEntity;
import com.toady.web.app.subscript.repository.SubscriptCommentRepositoryCustom;
import com.toady.web.app.user.entity.QUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * packageName    : com.toady.web.app.subscript.repository.impl
 * fileName       : SubscriptCommentRepositoryImpl
 * author         : youn
 * date           : 2022/06/09
 * description    : 청약 코멘트 repo
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/09        youn       최초 생성
 */
@Transactional(readOnly = true)
@Slf4j
public class SubscriptCommentRepositoryImpl extends QuerydslRepositorySupport implements SubscriptCommentRepositoryCustom {


    @Autowired
    private JPAQueryFactory queryFactory;


    public SubscriptCommentRepositoryImpl() {
        super(SubscriptCommentRepositoryImpl.class);
    }

    /**
     * 청약에 달린 댓글을 조회한다.
     * @param param
     * @return
     * @throws Exception
     */
    public List<Tuple> getSubscriptCommentList(SubscriptDTO.GetComment param) throws Exception {

        final QSubscriptEntity subscriptEntity = QSubscriptEntity.subscriptEntity;
        final QSubscriptCommentEntity subscriptComment = QSubscriptCommentEntity.subscriptCommentEntity;
        final QUserEntity userEntity = QUserEntity.userEntity;

        // subscriptSeq 로 houseManageNo 조회
        SubscriptEntity subscript = queryFactory.select(subscriptEntity).from(subscriptEntity).where(subscriptEntity.subscriptSeq.eq(param.getSubscriptSeq())).fetchOne();

        List<Tuple> result = queryFactory
                .select(
                        subscriptComment.subscriptCommentSeq
                        ,subscriptComment.subscriptSeq
                        ,subscriptComment.houseManageNo
                        ,subscriptComment.content
                        ,subscriptComment.recommend
                        ,subscriptComment.oppose
                        ,subscriptComment.parentSubscriptCommentSeq
                        ,subscriptComment.deleteYn
                        ,subscriptComment.createUserSeq
                        ,subscriptComment.createDt
                        ,subscriptComment.updateUserSeq
                        ,subscriptComment.updateDt
                        ,userEntity.userNicName
                )
                .from(subscriptComment)
                .leftJoin(userEntity)
                .on(subscriptComment.createUserSeq.eq(userEntity.userSeq))

                .where(
                        subscriptComment.houseManageNo.eq(subscript.getHouseManageNo()) // 아파트(특공,1순위,2순위 가 있음) 다 같은 1개로 봄.
                                .and(subscriptComment.deleteYn.eq("N"))
                )
                .orderBy(
                        subscriptComment.parentSubscriptCommentSeq.asc().nullsFirst(),
                        subscriptComment.createDt.asc()
                )
                .fetch();


        return result;
    }

}
