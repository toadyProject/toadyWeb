package com.toady.web.app.subscript.repository.impl;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.toady.web.app.subscript.dto.SubscriptDTO;
import com.toady.web.app.subscript.dto.SubscriptListDto;
import com.toady.web.app.subscript.entity.*;
import com.toady.web.app.subscript.repository.SubscriptRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.toady.web.app.subscript.entity.QSubscriptCompetitionRateEntity.subscriptCompetitionRateEntity;
import static com.toady.web.app.subscript.entity.QSubscriptCompetitionRateScoreEntity.subscriptCompetitionRateScoreEntity;
import static com.toady.web.app.subscript.entity.QSubscriptEntity.subscriptEntity;
import static com.toady.web.app.subscript.entity.QSubscriptSpecialCompetitionRateEntity.subscriptSpecialCompetitionRateEntity;

/**
 * packageName    : com.toady.web.app.subscript.repository.impl
 * fileName       : SubscriptCommentRepositoryImpl
 * author         : youn
 * date           : 2022/07/11
 * description    : 청약 목록 repo
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/07/11        youn       최초 생성
 */
@Transactional(readOnly = true)
public class SubscriptRepositoryImpl extends QuerydslRepositorySupport implements SubscriptRepositoryCustom {


    @Autowired
    private JPAQueryFactory queryFactory;

    public SubscriptRepositoryImpl() {
        super(SubscriptRepositoryImpl.class);
    }

    /**
     * 청약 목록을 조회한다.
     */
    public List<SubscriptListDto> getSubscriptList(SubscriptDTO.Search param, ArrayList<String> rceptSeAry){

        final QSubscriptCommentEntity subscriptComment = QSubscriptCommentEntity.subscriptCommentEntity;
        final QSubscriptCommentGroupByHouseManageNoEntity subSubscriptComment = QSubscriptCommentGroupByHouseManageNoEntity.subscriptCommentGroupByHouseManageNoEntity;

        List<SubscriptListDto> result = queryFactory
                .select(

                        Projections.fields(
                                SubscriptListDto.class,
                                subscriptEntity.subscriptSeq.as("subscriptSeq"),
                                subscriptEntity.houseNm.as("houseNm"),
                                subscriptEntity.inDate.as("inDate"),
                                subscriptEntity.rceptSe.as("rceptSe"),
                                subscriptEntity.restdeAt.as("restdeAt"),
                                subscriptEntity.pblancNo.as("pblancNo"),
                                subscriptEntity.inDay.as("inDay"),
                                subscriptEntity.rm.as("rm"),
                                subscriptEntity.houseSecd.as("houseSecd"),
                                subscriptEntity.houseManageNo.as("houseManageNo"),
                                subscriptEntity.detailRequestYn.as("detailRequestYn"),
                                subscriptEntity.supplyLocation.as("supplyLocation"),
                                subscriptEntity.supplyScale.as("supplyScale"),
                                subscriptEntity.salesOfficeContact.as("salesOfficeContact"),
                                subscriptEntity.recruitmentNoticeFileUrl.as("recruitmentNoticeFileUrl"),
                                subscriptEntity.upcomingMonth.as("upcomingMonth"),
                                subscriptEntity.applicationDate.as("applicationDate"),
                                subscriptEntity.specialPriorityArea.as("specialPriorityArea"),
                                subscriptEntity.specialOtherAreas.as("specialOtherAreas"),
                                subscriptEntity.specialPlaceOfReception.as("specialPlaceOfReception"),
                                subscriptEntity.specialReceiptPeriod.as("specialReceiptPeriod"),
                                subscriptEntity.onePriorityArea.as("onePriorityArea"),
                                subscriptEntity.oneOtherAreas.as("oneOtherAreas"),
                                subscriptEntity.onePlaceOfReception.as("onePlaceOfReception"),
                                subscriptEntity.oneReceiptPeriod.as("oneReceiptPeriod"),
                                subscriptEntity.twoPriorityArea.as("twoPriorityArea"),
                                subscriptEntity.twoOtherAreas.as("twoOtherAreas"),
                                subscriptEntity.twoPlaceOfReception.as("twoPlaceOfReception"),
                                subscriptEntity.twoReceiptPeriod.as("twoReceiptPeriod"),
                                subscriptEntity.housingDivision.as("housingDivision"),
                                subscriptEntity.winnerAnnouncementDate.as("winnerAnnouncementDate"),
                                subscriptEntity.contractDate.as("contractDate"),
                                subscriptEntity.subscriptDate.as("subscriptDate"),
                                subscriptEntity.developer.as("developer"), //시행사 : 코리아신탁(주)
                                subscriptEntity.contractor.as("contractor"), //시공사 : 에이치아이건설 주식회사
                                subscriptEntity.businessPhoneNumber.as("businessPhoneNumber"), //사업주체 전화번호 : 02-3430-2062
                                subscriptEntity.createDt.as("createDt"),
                                subscriptEntity.updateDt.as("updateDt"),
                                subSubscriptComment.cnt.as("subscriptCommentCnt")
                                /*
                                ExpressionUtils.as(
                                        JPAExpressions.select(count(subscriptComment.subscriptCommentSeq))
                                                .from(subscriptComment)
                                                .where(
                                                        subscriptComment.houseManageNo.eq(subscriptEntity.houseManageNo) // 아파트(특공,1순위,2순위 가 있음) 다 같은 1개로 봄.
                                                                .and(subscriptComment.deleteYn.eq("N"))
                                                ), "subscriptCommentCnt")
                                 */
                        )
                )
                .from(subscriptEntity)

                .leftJoin(subSubscriptComment)
                .on(subSubscriptComment.houseManageNo.eq(subscriptEntity.houseManageNo))

                .where(
                        subscriptEntity.inDate.between(param.getStartStr(), param.getEndStr())
                                .and(subscriptEntity.rceptSe.in(rceptSeAry))
                )

                .orderBy(
                        subscriptEntity.rceptSe.asc(),
                        subscriptEntity.createDt.asc()
                )
                .fetch();




        return result;
    }

    /**
     * 특별공급 경쟁률 조회.
     */
    public List<SubscriptSpecialCompetitionRateEntity> getSubscriptSpecialCompetitionRateList(Long subscriptCommentSeq, String rceptSe){

        return queryFactory
                .select(
                        subscriptSpecialCompetitionRateEntity
                )
                .from(subscriptSpecialCompetitionRateEntity)
                .where(subscriptSpecialCompetitionRateEntity.subscriptSeq.eq(subscriptCommentSeq),
                        subscriptSpecialCompetitionRateEntity.rceptSe.eq(rceptSe)
                        )
                .orderBy(
                        Expressions.stringTemplate("SUBSTRING_INDEX({0}, '.', 1)", subscriptSpecialCompetitionRateEntity.housingType).asc(),
                        Expressions.stringTemplate("REGEXP_REPLACE({0}, '[^a-zA-Z]', '')", subscriptSpecialCompetitionRateEntity.housingType).asc()
                )
                .fetch();
    }

    /**
     * 일반공급 경쟁률 조회
     */
    public List<SubscriptCompetitionRateEntity> getSubscriptCompetitionRateList(String houseManageNo){

        return queryFactory
                .select(
                        subscriptCompetitionRateEntity
                )
                .from(subscriptCompetitionRateEntity)
                .innerJoin(subscriptEntity).on(subscriptCompetitionRateEntity.subscriptSeq.eq(subscriptEntity.subscriptSeq))
                .where(subscriptEntity.houseManageNo.eq(houseManageNo))
                .orderBy(
                          subscriptCompetitionRateEntity.housingType.asc()
                        , Expressions.stringTemplate("SUBSTRING_INDEX({0}, '.', 1)", subscriptCompetitionRateEntity.housingType).asc()
                        , Expressions.stringTemplate("REGEXP_REPLACE({0}, '[^a-zA-Z]', '')", subscriptCompetitionRateEntity.housingType).asc()
                        , subscriptCompetitionRateEntity.rceptSe.asc()
                )
                .fetch();
    }

    /**
     * 경쟁률 가점 조회.
     */
    public List<SubscriptCompetitionRateScoreEntity> getSubscriptCompetitionRateScoreList(String houseManageNo){

        return queryFactory
                .select(
                        subscriptCompetitionRateScoreEntity
                )
                .from(subscriptCompetitionRateScoreEntity)
                .where(subscriptCompetitionRateScoreEntity.houseManageNo.eq(houseManageNo))
                .orderBy(
                        Expressions.stringTemplate("SUBSTRING_INDEX({0}, '.', 1)", subscriptCompetitionRateScoreEntity.housingType).asc(),
                        Expressions.stringTemplate("REGEXP_REPLACE({0}, '[^a-zA-Z]', '')", subscriptCompetitionRateScoreEntity.housingType).asc()
                )
                .fetch();
    }
}
