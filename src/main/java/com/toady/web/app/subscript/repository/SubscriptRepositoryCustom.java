package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.dto.SubscriptDTO;
import com.toady.web.app.subscript.dto.SubscriptListDto;
import com.toady.web.app.subscript.entity.SubscriptCompetitionRateEntity;
import com.toady.web.app.subscript.entity.SubscriptCompetitionRateScoreEntity;
import com.toady.web.app.subscript.entity.SubscriptSpecialCompetitionRateEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * packageName    : com.toady.web.app.subscript.repository
 * fileName       : SubscriptCommentRepositoryCustom
 * author         : youn
 * date           : 2022/07/11
 * description    : 청약 정보 목록 Custom.
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/07/11        youn       최초 생성
 */
public interface SubscriptRepositoryCustom {

    public List<SubscriptListDto> getSubscriptList(SubscriptDTO.Search param, ArrayList<String> rceptSeAry);

    /**
     * 특별공급 경쟁률 조회.
     * @return
     */
    public List<SubscriptSpecialCompetitionRateEntity> getSubscriptSpecialCompetitionRateList(Long subscriptCommentSeq, String rceptSe);

    /**
     * 일반공급 경쟁률 조회
     * @return
     */
    public List<SubscriptCompetitionRateEntity> getSubscriptCompetitionRateList(String houseManageNo);

    /**
     * 경쟁률 가점 조회.
     * @return
     */
    public List<SubscriptCompetitionRateScoreEntity> getSubscriptCompetitionRateScoreList(String houseManageNo);

}
