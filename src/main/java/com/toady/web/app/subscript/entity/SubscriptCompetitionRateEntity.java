package com.toady.web.app.subscript.entity;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptCompetitionRateEntity
 * author         : youn
 * date           : 2023/11/26
 * description    : 일반공급 경쟁률
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2023/11/26        youn       최초 생성
 */

import lombok.*;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@AllArgsConstructor //모든 필드 값을 파라미터로 받는 생성자를 만듦
@NoArgsConstructor
@Getter
@Setter
@DynamicInsert
@Table(
        name = "T_SUBSCRIPT_COMPETITION_RATE",
        uniqueConstraints = {
                @UniqueConstraint(
                        name="uk_subscriptCompetitionRate",
                        columnNames={"SUBSCRIPT_SEQ","HOUSING_TYPE","RCEPT_SE", "LOCATION_TYPE"}
                )
        }

)
public class SubscriptCompetitionRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptCompetitionRateSeq;

    @Column(name="SUBSCRIPT_SEQ", nullable = false, length = 20)
    private Long subscriptSeq;

    @Comment("주택형")
    @Column(name = "HOUSING_TYPE", length = 25)
    private String housingType; //주택형

    @Comment("공급세대수")
    @Column(name = "SUPPLY_CNT", length = 50)
    private String supplyCnt; // 공급세대수

    @Comment("01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대")
    @Column(name = "RCEPT_SE", nullable = false, length = 2)
    private String rceptSe;  //01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대

    @Comment("지역 구분")
    @Column(name = "LOCATION_TYPE", length = 20)
    private String locationType; // 지역 구분

    @Comment("접수 건 수")
    @Column(name = "SUBMISSION_CNT", length = 15)
    private String submissionCnt; //접수 건 수

    @Comment("순위내 경쟁률 (미달 세대 수)")
    @Column(name = "SUBMISSION_RATE_CNT", length = 15)
    private String submissionRateCnt; //순위내 경쟁률 (미달 세대 수)

    @Comment("청약 결과")
    @Column(name = "LOTTERY_RESULT", length = 11)
    private String lotteryResult; // 청약 결과

    @Comment("등록일시")
    private LocalDateTime createDt;

    @Comment("수정일시")
    private LocalDateTime updateDt;
}
