package com.toady.web.app.subscript.entity;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptEntity
 * author         : youn
 * date           : 2022/05/19
 * description    : 청약일정 및 정보
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@AllArgsConstructor //모든 필드 값을 파라미터로 받는 생성자를 만듦
@NoArgsConstructor
@Getter
@Setter
@DynamicInsert
@Table(
        name = "T_SUBSCRIPT_INFO",
        uniqueConstraints = {
                @UniqueConstraint(
                        name="uk_subscript",
                        columnNames={"IN_DATE", "RCEPT_SE", "HOUSE_MANAGE_NO"}
                )
        }

)
public class SubscriptEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptSeq;

    @Comment("청약이름")
    @Column(nullable = false, length = 150)
    private String houseNm;

    @Comment("청약일자")
    @Column(name="IN_DATE", nullable = false, length = 8)
    private String inDate;

    @Comment("01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대")
    @Column(name="RCEPT_SE", nullable = false, length = 2)
    private String rceptSe;

    @Column(nullable = true, length = 5)
    private String restdeAt;

    @Comment("청약 아파트 고유 번호")
    @Column(length = 10)
    private String pblancNo;

    @Column(length = 2)
    private String inDay;

    @Column(length = 50)
    private String rm;

    @Comment("(01:APT특별공급,1순위,2순위, 04:잔여세대), (03:민간임대), (02: 오피스텔)")
    @Column(length = 2)
    private String houseSecd;

    @Comment("청약 아파트 고유 번호")
    @Column(name="HOUSE_MANAGE_NO", length = 10)
    private String houseManageNo;

    @Comment("청약 상세정보 요청 유무")
    @Column(nullable = false, length = 1)
    @ColumnDefault("'N'")
    private String detailRequestYn;

    @Comment("공급위치")
    @Column(length = 150)
    private String supplyLocation;

    @Comment("공급규모")
    @Column( length = 50)
    private String supplyScale;

    @Comment("분양사무실 연락처")
    @Column(length = 150)
    private String salesOfficeContact;

    @Comment("모집공고문 fileUrl")
    @Column(length = 250)
    private String recruitmentNoticeFileUrl;

    @Comment("입주예정월")
    @Column(length = 250)
    private String upcomingMonth;

    @Comment("모집공고일")
    @Column(length = 30)
    private String applicationDate;

    @Comment("특별공급 해당지역")
    @Column(length = 30)
    private String specialPriorityArea;

    @Comment("특별공급 기타지역")
    @Column(length = 30)
    private String specialOtherAreas;

    @Comment("특별공급 접수장소")
    @Column(length = 50)
    private String specialPlaceOfReception;

    @Comment("특별공급 접수기간")
    @Column(length = 50)
    private String specialReceiptPeriod;

    @Comment("1순위 해당지역")
    @Column(length = 30)
    private String onePriorityArea;

    @Comment("1순위 기타지역")
    @Column(length = 30)
    private String oneOtherAreas;

    @Comment("1순위 접수장소")
    @Column(length = 50)
    private String onePlaceOfReception;

    @Comment("1순위 접수기간")
    @Column(length = 50)
    private String oneReceiptPeriod;

    @Comment("2순위 해당지역")
    @Column(length = 15)
    private String twoPriorityArea;

    @Comment("2순위 기타지역")
    @Column(length = 15)
    private String twoOtherAreas;

    @Comment("2순위 접수장소")
    @Column(length = 50)
    private String twoPlaceOfReception;

    @Comment("2순위 접수기간")
    @Column(length = 50)
    private String twoReceiptPeriod;

    @Comment("주택구분 : 민영")
    @Column(length = 10)
    private String housingDivision;

    @Comment("당첨자 발표일")
    @Column(length = 500)
    private String winnerAnnouncementDate;

    @Comment("계약일")
    @Column(length = 50)
    private String contractDate;

    @Comment("청약접수")
    @Column(length = 50)
    private String subscriptDate;

    /**
     * 입주자모집공고 기타사항
     */
    @Comment("시행사")
    @Column(length = 50)
    private String developer; //시행사 : 코리아신탁(주)

    @Comment("시공사")
    @Column(length = 50)
    private String contractor; //시공사 : 에이치아이건설 주식회사

    @Comment("사업주체 전화번호")
    @Column(length = 150)
    private String businessPhoneNumber; //사업주체 전화번호 : 02-3430-2062

    @Comment("최초등록일시")
    private LocalDateTime createDt;

    @Comment("수정일시")
    private LocalDateTime updateDt;



}
