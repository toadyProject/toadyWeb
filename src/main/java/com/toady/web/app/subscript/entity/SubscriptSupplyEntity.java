package com.toady.web.app.subscript.entity;



import lombok.*;
import org.hibernate.annotations.Comment;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptSupplyEntity
 * author         : youn
 * date           : 2022/05/19
 * description    : 입주자모집공고 공급대상
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(
        name = "T_SUBSCRIPT_SUPPLY",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "uk_subscriptSupply",
                        columnNames={"SUBSCRIPT_SEQ", "RCEPT_SE", "HOUSING_MODEL_NO", "HOUSING_TYPE"}
                )
        }
)
public class SubscriptSupplyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptSupplySeq;

    @Column(name="SUBSCRIPT_SEQ", nullable = false, length = 20)
    private Long subscriptSeq;

    @Comment("01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대")
    @Column(name = "RCEPT_SE", nullable = false, length = 2)
    private String rceptSe;

    @Comment("주택관리번호(모델번호)")
    @Column(name = "HOUSING_MODEL_NO", nullable = false, length = 25)
    private String housingModelNo;

    @Comment("주택형")
    @Column(name = "HOUSING_TYPE", length = 25)
    private String housingType;

    @Comment("군")
    @Column(length = 25)
    private String gun;

    @Comment("주택공급면적(주거전용+주거공용)")
    @Column(length = 25)
    private String housingSupplyArea;

    @Comment("특별공급_청년")
    @Column(length = 10)
    private String supplySpecialYoungMan;

    @Comment("특별공급_신혼부부")
    @Column(length = 10)
    private String supplySpecialHoneymoon;

    @Comment("특별공급_고령자")
    @Column(length = 10)
    private String supplySpecialTheAged;

    @Comment("공급세대수 일반")
    @Column(length = 50)
    private String supplyNormalCnt;

    @Comment("공급세대수 특별")
    @Column(length = 50)
    private String supplySpecialCnt;

    @Comment("계")
    @Column(length = 50)
    private String supplyTotalCnt;

    @Comment("비고")
    @Column(length = 300)
    private String note;

    @Comment("등록일시")
    private LocalDateTime createDt;

    @Comment("수정일시")
    private LocalDateTime updateDt;

}
