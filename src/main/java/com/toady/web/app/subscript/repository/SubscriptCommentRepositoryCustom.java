package com.toady.web.app.subscript.repository;

import com.querydsl.core.Tuple;
import com.toady.web.app.subscript.dto.SubscriptDTO;

import java.util.List;

/**
 * packageName    : com.toady.web.app.subscript.repository
 * fileName       : SubscriptCommentRepositoryCustom
 * author         : youn
 * date           : 2022/06/09
 * description    : 청약 정보 커멘트 Custom.
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/06/09        youn       최초 생성
 */
public interface SubscriptCommentRepositoryCustom {

    //청약 댓글 목록 조회.
    public List<Tuple> getSubscriptCommentList(SubscriptDTO.GetComment param) throws Exception;

}
