package com.toady.web.app.subscript.dto;

import com.toady.web.app.user.dto.UserDTO;
import com.toady.web.core.pagination.PageDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * packageName    : com.toady.web.app.subscript.dto
 * fileName       : SubscriptDto
 * author         : youn
 * date           : 2022/05/24
 * description    : 청약 일정 관련 DTO
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/24        youn       최초 생성
 */
public class SubscriptDTO {

    @Setter
    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor (access = AccessLevel.PROTECTED)
    public static class Search {

        @ApiModelProperty(value="조회할 검색 시작일", example="YYYYMMDD", required=true)
        private String startStr;

        @ApiModelProperty(value="조회할 검색 종료일", example="YYYYMMDD", required=true)
        private String endStr;

        @Builder.Default
        private String rceptSe01 = "Y";

        @Builder.Default
        private String rceptSe02 = "Y";

        @Builder.Default
        private String rceptSe03 = "Y";

        @Builder.Default
        private String rceptSe04 = "Y";

        @Builder.Default
        private String rceptSe05 = "Y";

        @Builder.Default
        private String rceptSe06 = "Y";

        @Builder.Default
        private String rceptSe07 = "Y";

        @Builder.Default
        private String rceptSe08 = "Y";

        @Builder.Default
        private String rceptSe09 = "Y";

        @Builder.Default
        private String rceptSe10 = "Y";

    }

    @Setter
    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor (access = AccessLevel.PROTECTED)
    public static class Detail {

        @NotNull
        private Long subscriptSeq;

        private Long userSeq;

    }

    @Setter
    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor (access = AccessLevel.PROTECTED)
    public static class GetComment extends PageDTO {

        @NotNull
        private Long subscriptSeq;

    }

    @Setter
    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor (access = AccessLevel.PROTECTED)
    public static class GetCommentDetail{

        @NotNull
        private Long subscriptCommentSeq;



    }


    @Setter
    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor (access = AccessLevel.PROTECTED)
    public static class SaveComment extends UserDTO.UserInfo{

        @NotNull(message = "청약 아파트 번호가 존재하지 않습니다.")
        private  String houseManageNo;

        @NotNull(message = "청약 번호가 존재하지 않습니다.")
        private Long subscriptSeq;

        //내용
        @NotBlank
        @NotNull
        @Size(min=1, max=3000, message = "글자는 최소 1자이상 3000자 이하로 등록 가능합니다.")
        private String content;

        //부모 SEQ
        private Long parentSubscriptCommentSeq;

    }

    @Setter
    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor (access = AccessLevel.PROTECTED)
    public static class UpdateComment extends UserDTO.UserInfo{

        @NotNull
        private Long subscriptCommentSeq;

        //내용
        private String content;

        @Builder.Default
        private String deleteYn = "N";

    }

}
