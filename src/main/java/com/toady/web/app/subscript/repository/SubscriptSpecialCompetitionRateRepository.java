package com.toady.web.app.subscript.repository;

import com.toady.web.app.subscript.entity.SubscriptSpecialCompetitionRateEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 특별공급 경쟁률
 */
public interface SubscriptSpecialCompetitionRateRepository extends JpaRepository<SubscriptSpecialCompetitionRateEntity,Long> {
//SubscriptRepositoryCustom
    /**
     * 입주자모집공고 공급대상 정보 조회.
     */
    public List<SubscriptSpecialCompetitionRateEntity> findBySubscriptSeqAndRceptSeOrderByHousingTypeAsc(Long subscriptSeq, String rceptSe);

}
