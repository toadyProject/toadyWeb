package com.toady.web.app.subscript.dto;

import com.querydsl.core.annotations.QueryProjection;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * packageName    : com.toady.web.app.subscript.dto
 * fileName       : SubscriptListDto
 * author         : youn
 * date           : 2022/07/12
 * description    :
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/07/12        youn       최초 생성
 */
@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class SubscriptListDto {

    private Long subscriptSeq;

    private String houseNm;

    private String inDate;

    private String rceptSe;

    private String restdeAt;

    private String pblancNo;

    private String inDay;

    private String rm;

    private String houseSecd;

    private String houseManageNo;

    private String detailRequestYn;

    private String supplyLocation;

    private String supplyScale;

    private String salesOfficeContact;

    private String recruitmentNoticeFileUrl;

    private String upcomingMonth;

    private String applicationDate;

    private String specialPriorityArea;

    private String specialOtherAreas;

    private String specialPlaceOfReception;

    private String specialReceiptPeriod;

    private String onePriorityArea;

    private String oneOtherAreas;

    private String onePlaceOfReception;

    private String oneReceiptPeriod;

    private String twoPriorityArea;

    private String twoOtherAreas;

    private String twoPlaceOfReception;

    private String twoReceiptPeriod;

    private String housingDivision;

    private String winnerAnnouncementDate;

    private String contractDate;

    private String subscriptDate;

    private String developer; //시행사 : 코리아신탁(주)

    private String contractor; //시공사 : 에이치아이건설 주식회사

    private String businessPhoneNumber; //사업주체 전화번호 : 02-3430-2062

    private LocalDateTime createDt;

    private LocalDateTime updateDt;

    private Long subscriptCommentCnt;

    @QueryProjection
    public SubscriptListDto(Long subscriptSeq, String houseNm, String inDate, String rceptSe, String restdeAt, String pblancNo, String inDay, String rm, String houseSecd, String houseManageNo, String detailRequestYn, String supplyLocation, String supplyScale, String salesOfficeContact, String recruitmentNoticeFileUrl, String upcomingMonth, String applicationDate, String specialPriorityArea, String specialOtherAreas, String specialPlaceOfReception, String specialReceiptPeriod, String onePriorityArea, String oneOtherAreas, String onePlaceOfReception, String oneReceiptPeriod, String twoPriorityArea, String twoOtherAreas, String twoPlaceOfReception, String twoReceiptPeriod, String housingDivision, String winnerAnnouncementDate, String contractDate, String subscriptDate, String developer, String contractor, String businessPhoneNumber, LocalDateTime createDt, LocalDateTime updateDt, Long subscriptCommentCnt) {
        this.subscriptSeq = subscriptSeq;
        this.houseNm = houseNm;
        this.inDate = inDate;
        this.rceptSe = rceptSe;
        this.restdeAt = restdeAt;
        this.pblancNo = pblancNo;
        this.inDay = inDay;
        this.rm = rm;
        this.houseSecd = houseSecd;
        this.houseManageNo = houseManageNo;
        this.detailRequestYn = detailRequestYn;
        this.supplyLocation = supplyLocation;
        this.supplyScale = supplyScale;
        this.salesOfficeContact = salesOfficeContact;
        this.recruitmentNoticeFileUrl = recruitmentNoticeFileUrl;
        this.upcomingMonth = upcomingMonth;
        this.applicationDate = applicationDate;
        this.specialPriorityArea = specialPriorityArea;
        this.specialOtherAreas = specialOtherAreas;
        this.specialPlaceOfReception = specialPlaceOfReception;
        this.specialReceiptPeriod = specialReceiptPeriod;
        this.onePriorityArea = onePriorityArea;
        this.oneOtherAreas = oneOtherAreas;
        this.onePlaceOfReception = onePlaceOfReception;
        this.oneReceiptPeriod = oneReceiptPeriod;
        this.twoPriorityArea = twoPriorityArea;
        this.twoOtherAreas = twoOtherAreas;
        this.twoPlaceOfReception = twoPlaceOfReception;
        this.twoReceiptPeriod = twoReceiptPeriod;
        this.housingDivision = housingDivision;
        this.winnerAnnouncementDate = winnerAnnouncementDate;
        this.contractDate = contractDate;
        this.subscriptDate = subscriptDate;
        this.developer = developer;
        this.contractor = contractor;
        this.businessPhoneNumber = businessPhoneNumber;
        this.createDt = createDt;
        this.updateDt = updateDt;
        this.subscriptCommentCnt = subscriptCommentCnt;
    }
}
