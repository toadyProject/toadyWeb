package com.toady.web.app.subscript.controllor;

import com.toady.web.app.subscript.dto.SubscriptDTO;
import com.toady.web.app.subscript.dto.SubscriptListDto;
import com.toady.web.app.subscript.service.SubscriptService;
import com.toady.web.core.constants.ResultCode;
import com.toady.web.core.response.ResData;
import com.toady.web.core.security.service.CustomUserDetails;
import com.toady.web.core.util.CmnUtils;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * packageName    : com.toady.web.app.subscript.controllor
 * fileName       : SubscriptController
 * author         : youn
 * date           : 2022/05/24
 * description    : 청약일정 컨트롤러
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/24        youn       최초 생성
 */
@Api(tags = "청약일정 API")
@RestController
@RequiredArgsConstructor // 자동주입
@RequestMapping("/webApi/subscript")
@Slf4j
public class SubscriptController {

    private final SubscriptService subscriptService;

    /**
     * 청약 일정 목록을 조회한다.
     * @return 청약 일정
     */
    @Operation(summary = "정약 일정 목록 조회", description = "청약 일정 목록 조회", tags = "청약일정 API")
    @GetMapping("/getSubscriptList")
    public ResponseEntity<ResData> getSubscriptList(@Valid SubscriptDTO.Search param,
                                                    BindingResult result
    ){

        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResData.of(ResultCode.VALID_ERROR.getCode(), CmnUtils.getErrMsg(result, '\n')));
        }

        //화면에 넘기기는 Map 생성.
        Map<String, Object> rtnMap = new HashMap<>();

        List<SubscriptListDto> subscriptList = subscriptService.getSubscriptList(param);

        //resultList를 담아서 넘김.
        rtnMap.put("resultList", subscriptList);

        return ResponseEntity.ok().body(ResData.of(rtnMap, ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage()));
    }

    /**
     * 청약 셍세 정보를 조회한다.
     * @return 청약 상세 정보.
     */
    @Operation(summary = "청약 셍세 정보 조회", description = "청약 셍세 정보", tags = "청약일정 API")
    @GetMapping("/getSubscriptDetail")
    public ResponseEntity<ResData> getSubscriptDetail(
            @Valid SubscriptDTO.Detail param,
            BindingResult result,
            @AuthenticationPrincipal CustomUserDetails user,
            HttpServletRequest request
    ){

        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResData.of(ResultCode.VALID_ERROR.getCode(), CmnUtils.getErrMsg(result, '\n')));
        }

        if(user != null){
            param.setUserSeq(user.getUserSeq());
        }

        //청약 셍사정보를 조회한다.
        Map<String, Object> rtnMap = subscriptService.getSubscriptDetail(param, request);


        return ResponseEntity.ok().body(ResData.of(rtnMap, ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage()));
    }

    /**
     * 청약 일정에 달린 커멘트 목록을 조회한다.
     * @param param
     * @param result
     * @return 커멘트 정보.
     */
    @Operation(summary = "청약 일정에 달린 커멘트 목록 조회", description = "청약 일정에 달린 커멘트 목록", tags = "청약일정 API")
    @GetMapping("/getSubscriptComment")
    public ResponseEntity<ResData> getSubscriptComment(@Valid SubscriptDTO.GetComment param,
                                                      BindingResult result
    ){
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResData.of(ResultCode.VALID_ERROR.getCode(), CmnUtils.getErrMsg(result, '\n')));
        }

        try {

            //청약 댓글 목록을 조회한다.
            Map<String, Object> rtnMap = subscriptService.getSubscriptComment(param);

            return ResponseEntity.ok().body(ResData.of(rtnMap, ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage()));

        } catch (Exception e ){
            return ResponseEntity.ok().body(ResData.of(ResultCode.SYSTEM_ERROR.getCode(), ResultCode.SYSTEM_ERROR.getMessage()));
        }


    }

    /**
     * 청약 댓글 등록.
     * @param param
     * @param result
     * @return 결과.
     */
    @Operation(summary = "청약 댓글 등록", description = "청약 댓글 등록", tags = "청약일정 API")
    @Secured({"ROLE_USER"})
    @PostMapping("/saveSubscriptComment")
    public ResponseEntity<ResData> saveSubscriptComment(@Valid @RequestBody SubscriptDTO.SaveComment param,
                                                        @AuthenticationPrincipal CustomUserDetails user,
                                                       BindingResult result
    ){

        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResData.of(ResultCode.VALID_ERROR.getCode(), CmnUtils.getErrMsg(result, '\n')));
        }

        //사용자 정보를 넣는다.
        param.setUserNicName(user.getUserNicName());
        param.setUserEmail(user.getUsername());
        param.setUserSeq(user.getUserSeq());

        //청약에 코멘트 등록.
        return subscriptService.saveSubscriptComment(param);
    }


    /**
     * 청약 일정에 달린 커멘트를 상세 조회한다.
     * @param param
     * @param result
     * @return 커멘트 정보.
     */
    @Operation(summary = "청약 일정에 달린 커멘트를 상세 조회", description = "청약 일정에 달린 커멘트를 상세", tags = "청약일정 API")
    @PostMapping("/getSubscriptCommentDetail")
    public ResponseEntity<ResData> getSubscriptCommentDetail(
            @Valid @RequestBody SubscriptDTO.GetCommentDetail param,
            BindingResult result
    ){
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResData.of(ResultCode.VALID_ERROR.getCode(), CmnUtils.getErrMsg(result, '\n')));
        }

        //청약 댓글 상세 조회
        return subscriptService.getSubscriptCommentDetail(param);
    }

    /**
     * 청약 코멘트 수정.
     * @param param
     * @param result
     * @return 결과.
     */
    @Operation(summary = "청약 코멘트 수정", description = "청약 코멘트 수정", tags = "청약일정 API")
    @Secured({"ROLE_USER"})
    @PostMapping("/updateSubscriptComment")
    public ResponseEntity<ResData> updateSubscriptComment(@Valid @RequestBody SubscriptDTO.UpdateComment param,
                                                        @AuthenticationPrincipal CustomUserDetails user,
                                                        BindingResult result
    ){

        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResData.of(ResultCode.VALID_ERROR.getCode(), CmnUtils.getErrMsg(result, '\n')));
            // throw new BaseException(CmnUtils.getErrMsg(result, '\n'), HttpStatus.BAD_REQUEST);
        }

        //사용자 정보를 넣는다.
        param.setUserNicName(user.getUserNicName());
        param.setUserEmail(user.getUsername());
        param.setUserSeq(user.getUserSeq());

        //청약에 코멘트 등록.
        return subscriptService.updateSubscriptComment(param);
    }

}
