package com.toady.web.app.subscript.entity;

/**
 * packageName    : com.toady.web.app.subscript.entity
 * fileName       : SubscriptSpecialEntity
 * author         : youn
 * date           : 2022/05/19
 * description    : 입주자모집공고 특별공급 공급대상
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022/05/19        youn       최초 생성
 */

import lombok.*;
import org.hibernate.annotations.Comment;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(
        name = "T_SUBSCRIPT_SPECIAL",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "uk_subscriptSpecial",
                        columnNames = {"SUBSCRIPT_SEQ", "RCEPT_SE", "SPECIAL_HOUSING_TYPE"}
                )
        }
)
public class SubscriptSpecialEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, length = 20)
    private Long subscriptSpecialSeq;

    @Column(name="SUBSCRIPT_SEQ", nullable = false, length = 20)
    private Long subscriptSeq;

    @Comment("01:APT 특별공금, 02:1순위, 03:2순위, 04:민간임대, 05:오피스텔, 06:잔여세대")
    @Column(name = "RCEPT_SE", nullable = false, length = 2)
    private String rceptSe;

    @Comment("주택형")
    @Column(name="SPECIAL_HOUSING_TYPE", nullable = false, length = 15)
    private String specialHousingType;

    @Comment("다자녀가구")
    @Column(length = 11)
    private String specialMultiFamily;

    @Comment("신혼부부")
    @Column(length = 11)
    private String specialHoneymoon;

    @Comment("생애최초")
    @Column(length = 11)
    private String specialFirstTimeInLife;

    @Comment("노부모 부양")
    @Column(length = 11)
    private String specialOldParents;

    @Comment("기관추천")
    @Column(length = 11)
    private String specialRecommendation;

    @Comment("이전기관")
    @Column(length = 11)
    private String specialFormerInstitution;

    @Comment("기타")
    @Column(length = 50)
    private String specialOther;

    @Comment("등록일시")
    private LocalDateTime createDt;

    @Comment("수정일시")
    private LocalDateTime updateDt;

}
