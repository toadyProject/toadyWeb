package com.toady.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@SpringBootTest
class MainApplicationTests {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Test
	public void subscriptTest() {
		// TODO Auto-generated method stub
		String text = "aaaaa1";

		Pattern passPattern1 = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"); //6자 영문+특문+숫자
		Matcher passMatcher = passPattern1.matcher(text);

		if (!passMatcher.find()) {
			System.out.println("비밀번호는 영문+숫자 6자리 이상으로 구성되어야 합니다");
		}

	}

}
