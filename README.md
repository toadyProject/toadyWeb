# TOADY(두껍아) Project
- 두껍아! 두껍아! 헌집 줄께 새집 다오.
- 개인 프로젝트.

> # 프로젝트 개요<br/>
| 구분 | 내용 |
| ------ | ------ |
| 기획 서비스 | 부동산, 청약 정보 공유 플랫폼. |
| 기획 배경 | 청약일정(청약홈) 및 공급아파트 위치(네이버 주소검색), 부동산 카페, 주변시세(호갱노노, 네이버 부동산 등) 각각 다른 사이트에서 정보를 조회 한다. <br/> 위의 정보를 한군데서 볼 수 있도록 편의성을 제공한다. |
|기획 목적|청약 일정 및 부동산 정보를 한군데서 볼수있는 서비스 제공.|
|기대 효과|가입자 수가 100만이 넘는 부동산 카페 및 지역별 중소형 부동산 카페만 수백곳.<br/> 호갱노노 회원수 약 200만.<br/> 부동산 시세 정보 플랫폼 및 부동산 카페 인원 흡수.|
|주요 기능|청약일정 제공.<br/> 특정 지역 청약 정보 알림.<br/> 청약 일정에 댓글 기능 청약 공급 아파트에 대한 위치 지도로 표시.<br/> 청약 공급 주변의 시세 정보 지도 및 표로 통계 표시.<br/> 커뮤니티 공간 제공.|
|주요 고객|광고주 및 회원 수 증가에 따른 동적 비즈니스 타겟 설정.|
|도메인|RestApi Domain : https://api.toady.co.kr<br/> WebSite : https://www.toady.co.kr |

> # 사이트<br/>

![KakaoTalk_Photo_2022-07-14-13-13-29](/uploads/bd94f337d704f0b9f5bb975e92166c60/KakaoTalk_Photo_2022-07-14-13-13-29.jpeg)
![KakaoTalk_Photo_2022-07-14-13-09-18_003](/uploads/f1d323192eb5baa69ca9d4c6ce8124ea/KakaoTalk_Photo_2022-07-14-13-09-18_003.jpeg)
![KakaoTalk_Photo_2022-07-14-13-09-18_002](/uploads/31accd5e7c899de2a49062eb8c049283/KakaoTalk_Photo_2022-07-14-13-09-18_002.jpeg)
![KakaoTalk_Photo_2022-07-14-13-09-18_001](/uploads/1e63c19b94510c759714171c048b00f0/KakaoTalk_Photo_2022-07-14-13-09-18_001.jpeg)


> # 사양<br/>
* 서버 : Ubuntu 22.04 LTS (GNU/Linux 5.15.0-33-generic x86_64) <br/>
* 가상화 도구 : Docker <br/>
* 웹서버 : Nginx:1.21.5<br/>
* 데이터베이스 : mariadb:10.3<br/>

> # Backend 언어 및 주요 프레임 워크.<br/>
* JAVA 11 <br/>
* Spring Boot 5.0 <br/>
* Spring Security  <br/>
* JPA<br/>
* Querydsl 1.0.10<br/>
* JWT:0.9.1<br/>
* lombok<br/>

> # 기능 및 Api

* 공통 RESPONSE BODY. <br/>
  * resultCode : "SUCCESS" 아니면 전부 실패.<br/>
  * resultMessage": 결과 메시지.<br/>
  * data : resultCode가 SUCCESS일 경우만 data가 들어있다.<br/>

<br/>

* 로그인. <br/>
  * 최초 로그인시 data.accessToken 값을 준다.<br/>
  * 90분간 유효한 토큰을 발행한다.<br/>

* 받은 값 예 : <br/>
{<br/>
    "data": {<br/>
        "accessToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0QG5hdmVyL....."<br/>
    },<br/>
    "resultCode": "SUCCESS",<br/>
    "resultMessage: "로그인 성공"<br/>
}<br/>
<br/>

* 로그인이 필요한 서비스 접근시.
  * Authorization에 토큰값을 넣을때 "Bearer"을 넣고 한칸 띈 후 토큰값을 넣는다.<br/>
  * 서버에서 token값을 확인하여 30분 이하로 남았을 경우 휴효시간 90분을 갱신하여 Header의 Authorization에 토큰값을 넣어준다.<br/>
  * 30분 이상으로 남았다면 서버에서 Header의 Authorization에 기존 토큰값을 넣어준다. <br/>
<br/>

* 보낼 값 예 :<br/>
Request HEADER 에 아래와같이 토큰을 넣어줘야 한다.<br/>
{<br/>
    "Content-Type": "application/json",<br/>
    "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0QG5hdmVyL....."<br/>
}
<br/>
<br/>

* **기능 표**

| 기능 번호 | 기능 | 로그인 필요 여부 |
| ------ | ------ | ------ |
| 1 | 회원가입 | X |
| 2 | 이메일 인증 | X |
| 3 | 로그인 | X |
| 4 | 청약일정 일정 리스트 조회 | X |
| 5 | 청약일정 상세 조회 | X |
| 6 | 청약일정 상세 코멘트 조회 | X |
| 7 | 청약일정 상세 코멘트 등록 | O |
| 8 | 청약일정 상세 코멘트 수정 | O |
| 9 | 청약일정 상세 코멘트 삭제 | O |

* **Request 표**

| 기능 번호 | URL | METHOD | REQUEST HEADER | REQUEST BODY | 
| ------ | ------ | ------ | ------ | ------ | 
| 1 | https://api.toady.co.kr/webApi/user/regUser | POST | { <br/> 'Content-Type': 'application/json' <br/>} | { <br/> "userEmail":"test@naver.co.kr",<br/> "userPassword": "test123456",<br/> "userPasswordConfirm":"test123456",<br/> "userNicName":"테스터" <br/> } | 
| 2 | https://api.toady.co.kr/webApi/certify/certifyCheck | GET/POST | "Content-Type": "application/x-www-urlencoded" | userEmail=test@naver.co.kr<br/>&<br/>certifyCode=AT1-KRPF3-LFP | 
| 3 | https://api.toady.co.kr/webApi/user/login | POST | { <br/> 'Content-Type': 'application/json' <br/>}  | {<br/>"userEmail": "test@naver.co.kr",<br/> "userPassword": "test123456"<br/> } | 
| 4 | https://api.toady.co.kr/webApi/subscript/getSubscriptList | POST | { <br/> 'Content-Type': 'application/json' <br/>} | { <br/> "startStr" : "202205",<br/> "endStr":"202206" <br/>} | 
| 5 | https://api.toady.co.kr/webApi/subscript/getSubscriptDetail | POST | { <br/> 'Content-Type': 'application/json' <br/>} |{ <br/>"subscriptSeq" : 6505 <br/>}  | 
| 6 | https://api.toady.co.kr/webApi/subscript/getSubscriptComment | POST | { <br/> 'Content-Type': 'application/json' <br/>} | {<br/> "subscriptSeq": 6505,<br/> "pageNumber":1,<br/> "recordPerCount": 10<br/> } | 
| 7 | https://api.toady.co.kr/webApi/subscript/saveSubscriptComment | POST | { <br/> 'Content-Type': 'application/json', 'Authorization':'Bearer [넘겨받은 토큰 값]' <br/>} | {<br/> "subscriptSeq":"6505",<br/> "content":"사진 하늘채 리센티아 가격이 비싸게 나온것 같은데 어떠신가요? ㅜ.ㅜ"<br/>} | 
| 8 | https://api.toady.co.kr/webApi/subscript/updateSubscriptComment | POST |  { <br/> 'Content-Type': 'application/json', 'Authorization':'Bearer [넘겨받은 토큰 값]' <br/>} | { <br/>"subscriptCommentSeq": 76,<br/>"content":"아 다시보니까 그렇게 안비싼것 같아요 ㅎㅎ" <br/>} | 
| 9 | https://api.toady.co.kr/webApi/subscript/updateSubscriptComment | POST |  { <br/> 'Content-Type': 'application/json', 'Authorization':'Bearer [넘겨받은 토큰 값]' <br/>} | { <br/>"subscriptCommentSeq": 76, <br/>"deleteYn":"Y"<br/>} | 
<br/>

* **Response 표**

| 기능 번호 | RESPONSE HEADER | RESPONSE BODY |
| ------ | ------ | ------ |
| 1 | { <br/> 'Content-Type': 'application/json' <br/>}  | {<br/> "data": null,<br/>"resultCode": "SUCCESS",<br/>"resultMessage": "성공적으로 회원가입 되었습니다. [test@naver.co.kr]로 인증 URL을 전송했습니다 인증 후 로그인 가능합니다."<br/> } |
| 2 | { <br/> 'Content-Type': 'application/json' <br/>}  |  {"data":null,"resultCode":"SUCCESS","resultMessage":"성공"} |
| 3 | { <br/> 'Content-Type': 'application/json' <br/>}  | {<br/>"data": {<br/>"accessToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0QG5hdmVyL....."<br/>},<br/>"resultCode": "SUCCESS",<br/>"resultMessage": "로그인 성공"<br/>}  |
| 4 | { <br/> 'Content-Type': 'application/json' <br/>}  | {<br/> "data": {<br/> "resultList": [{<br/> "subscriptSeq": 6393,<br/> "houseNm": "봉담 파라곤",<br/> "inDate": "20220602",<br/> "rceptSe": "02",<br/> "restdeAt": "N",<br/> "pblancNo": "2022000104",<br/> "inDay": "2",<br/> "rm": null,<br/> "houseSecd": "01",<br/> "houseManageNo": "2022000104",<br/> "detailRequestYn": "Y",<br/> "supplyLocation": "경기도 화성시 봉담읍 동화리 228번지 일원(동화3지구 지역주택조합 공동주택 건설사업)",<br/> "supplyScale": "105세대",<br/> "salesOfficeContact": "",<br/> "recruitmentNoticeFileUrl": "https://www.applyhome.co.kr/https://static.applyhome.co.kr/ai/aia/getAtchmnfl.do?houseManageNo=2022000104&pblancNo=2022000104&atchmnflSeqNo=700048&atchmnflSn=7",<br/> "upcomingMonth": "2023.05",<br/> "applicationDate": "2022-05-20 (아시아경제)",<br/> "onePriorityArea": "2022-05-31",<br/> "oneOtherAreas": "2022-06-02",<br/> "onePlaceOfReception": "인터넷",<br/> "twoPriorityArea": "2022-06-03",<br/> "twoOtherAreas": "2022-06-03",<br/> "twoPlaceOfReception": "인터넷",<br/> "winnerAnnouncementDate": "2022-06-10 ( <a href=\"http://bdparagon.com\" target=\"_blank\"><span style=\"color: #0e4eb2; position: relative;\">http://bdparagon.com</span></a> )",<br/> "contractDate": "2022-06-21 ~ 2022-06-23",<br/> "subscriptDate": null,<br/> "createDt": "2022-06-03T20:23:47",<br/> "updateDt": "2022-06-03T20:23:48"<br/> }]<br/> },<br/> "resultCode": "SUCCESS",<br/> "resultMessage": "성공"<br/> } |
| 5 | { <br/> 'Content-Type': 'application/json' <br/>}  | {<br/> "data": {<br/> "subscriptSupply": [<br/>{<br/>"subscriptSupplySeq": 33852,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"housingModelNo": "059.9044",<br/>"housingType": "78.5107",<br/>"gun": null,<br/>"housingSupplyArea": "14",<br/>"supplySpecialYoungMan": null,<br/>"supplySpecialHoneymoon": null,<br/>"supplySpecialTheAged": null,<br/>"supplyNormalCnt": "16",<br/>"supplySpecialCnt": "30",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSupplySeq": 33853,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"housingModelNo": "059.9044B",<br/>"housingType": "78.5378",<br/>"gun": null,<br/>"housingSupplyArea": "1",<br/>"supplySpecialYoungMan": null,<br/>"supplySpecialHoneymoon": null,<br/>"supplySpecialTheAged": null,<br/>"supplyNormalCnt": "0",<br/>"supplySpecialCnt": "1",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSupplySeq": 33854,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"housingModelNo": "084.8908A",<br/>"housingType": "111.4620",<br/>"gun": null,<br/>"housingSupplyArea": "2",<br/>"supplySpecialYoungMan": null,<br/>"supplySpecialHoneymoon": null,<br/>"supplySpecialTheAged": null,<br/>"supplyNormalCnt": "0",<br/>"supplySpecialCnt": "2",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSupplySeq": 33855,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"housingModelNo": "084.8978B",<br/>"housingType": "109.2578",<br/>"gun": null,<br/>"housingSupplyArea": "19",<br/>"supplySpecialYoungMan": null,<br/>"supplySpecialHoneymoon": null,<br/>"supplySpecialTheAged": null,<br/>"supplyNormalCnt": "21",<br/>"supplySpecialCnt": "40",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSupplySeq": 33856,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"housingModelNo": "084.9102C",<br/>"housingType": "109.0162",<br/>"gun": null,<br/>"housingSupplyArea": "19",<br/>"supplySpecialYoungMan": null,<br/>"supplySpecialHoneymoon": null,<br/>"supplySpecialTheAged": null,<br/>"supplyNormalCnt": "17",<br/>"supplySpecialCnt": "36",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>}<br/>],<br/>"subscriptNumberTwo": [<br/>{<br/>"subscriptNumberTwoSeq": 33852,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"twoHousingType": "059.9044",<br/>"gun": null,<br/>"subscriptAmount": null,<br/>"twoSupplyAmount": "48,100",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptNumberTwoSeq": 33853,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"twoHousingType": "059.9044B",<br/>"gun": null,<br/>"subscriptAmount": null,<br/>"twoSupplyAmount": "43,300",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptNumberTwoSeq": 33854,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"twoHousingType": "084.8908A",<br/>"gun": null,<br/>"subscriptAmount": null,<br/>"twoSupplyAmount": "68,400",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptNumberTwoSeq": 33855,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"twoHousingType": "084.8978B",<br/>"gun": null,<br/>"subscriptAmount": null,<br/>"twoSupplyAmount": "67,000",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptNumberTwoSeq": 33856,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"twoHousingType": "084.9102C",<br/>"gun": null,<br/>"subscriptAmount": null,<br/>"twoSupplyAmount": "65,900",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>}<br/>],<br/>"subscriptSpecial": [<br/>{<br/>"subscriptSpecialSeq": 25216,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"specialHousingType": "059.9044",<br/>"specialMultiFamily": "3",<br/>"specialHoneymoon": "6",<br/>"specialFirstTimeInLife": "3",<br/>"specialOldParents": "1",<br/>"specialRecommendation": "3",<br/>"specialFormerInstitution": "0",<br/>"specialOther": "0",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSpecialSeq": 25217,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"specialHousingType": "059.9044B",<br/>"specialMultiFamily": "0",<br/>"specialHoneymoon": "0",<br/>"specialFirstTimeInLife": "0",<br/>"specialOldParents": "0",<br/>"specialRecommendation": "0",<br/>"specialFormerInstitution": "0",<br/>"specialOther": "0",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSpecialSeq": 25218,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"specialHousingType": "084.8908A",<br/>"specialMultiFamily": "0",<br/>"specialHoneymoon": "0",<br/>"specialFirstTimeInLife": "0",<br/>"specialOldParents": "0",<br/>"specialRecommendation": "0",<br/>"specialFormerInstitution": "0",<br/>"specialOther": "0",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSpecialSeq": 25219,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"specialHousingType": "084.8978B",<br/>"specialMultiFamily": "4",<br/>"specialHoneymoon": "8",<br/>"specialFirstTimeInLife": "4",<br/>"specialOldParents": "1",<br/>"specialRecommendation": "4",<br/>"specialFormerInstitution": "0",<br/>"specialOther": "0",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>},<br/>{<br/>"subscriptSpecialSeq": 25220,<br/>"subscriptSeq": 6505,<br/>"rceptSe": "03",<br/>"specialHousingType": "084.9102C",<br/>"specialMultiFamily": "3",<br/>"specialHoneymoon": "7",<br/>"specialFirstTimeInLife": "3",<br/>"specialOldParents": "1",<br/>"specialRecommendation": "3",<br/>"specialFormerInstitution": "0",<br/>"specialOther": "0",<br/>"createDt": "2022-06-10T10:00:01",<br/>"updateDt": null<br/>}<br/>],<br/>"subscriptDetail": {<br/>"subscriptSeq": 6505,<br/>"houseNm": "사직 하늘채 리센티아",<br/>"inDate": "20220623",<br/>"rceptSe": "03",<br/>"restdeAt": "N",<br/>"pblancNo": "2022000309",<br/>"inDay": "23",<br/>"rm": null,<br/>"houseSecd": "01",<br/>"houseManageNo": "2022000309",<br/>"detailRequestYn": "Y",<br/>"supplyLocation": "부산광역시 동래구 사직동 566-3번지 일원",<br/>"supplyScale": "109세대",<br/>"salesOfficeContact": "",<br/>"recruitmentNoticeFileUrl": "https://www.applyhome.co.kr/https://static.applyhome.co.kr/ai/aia/getAtchmnfl.do?houseManageNo=2022000309&pblancNo=2022000309&atchmnflSeqNo=720266&atchmnflSn=4",<br/>"upcomingMonth": "2024.05",<br/>"applicationDate": "2022-06-10 (부산제일경제신문)",<br/>"onePriorityArea": "2022-06-21",<br/>"oneOtherAreas": "2022-06-22",<br/>"onePlaceOfReception": "인터넷",<br/>"twoPriorityArea": "2022-06-23",<br/>"twoOtherAreas": "2022-06-23",<br/>"twoPlaceOfReception": "인터넷",<br/>"winnerAnnouncementDate": "2022-06-29 ( <a href=\"http://www.사직하늘채리센티아.com\" target=\"_blank\"><span style=\"color: #0e4eb2; position: relative;\">http://www.사직하늘채리센티아.com</span></a> )",<br/>"contractDate": "2022-07-11 ~ 2022-07-13",<br/>"subscriptDate": null,<br/>"createDt": "2022-06-10T10:00:00",<br/>"updateDt": "2022-06-10T10:00:01"<br/>}<br/>},<br/>"resultCode": "SUCCESS",<br/>"resultMessage": "성공"<br/>} |
| 6 | { <br/> 'Content-Type': 'application/json' <br/>}  |  {<br/>"data": {<br/>"subscriptCommentList": {<br/>"content": [<br/>{<br/>"subscriptCommentSeq": 76,<br/>"subscriptSeq": 6505,<br/>"content": "아 다시보니까 그렇게 안비싼것 같아요 ㅎㅎ",<br/>"recommend": 0,<br/>"oppose": 0,<br/>"parentSubscriptCommentSeq": null,<br/>"deleteYn": "N",<br/>"createUserSeq": 35,<br/>"createDt": "2022-06-11T22:25:14.303675",<br/>"updateUserSeq": 35,<br/>"updateDt": "2022-06-11T22:26:58.152601"<br/>}<br/>],<br/>"pageable": {<br/>"sort": {<br/>"sorted": false,<br/>"unsorted": true,<br/>"empty": true<br/>},<br/>"pageNumber": 0,<br/>"pageSize": 10,<br/>"offset": 0,<br/>"paged": true,<br/>"unpaged": false<br/>},<br/>"totalPages": 1,<br/>"totalElements": 1,<br/>"last": true,<br/>"numberOfElements": 1,<br/>"size": 10,<br/>"number": 0,<br/>"sort": {<br/>"sorted": false,<br/>"unsorted": true,<br/>"empty": true<br/>},<br/>"first": true,<br/>"empty": false<br/>}<br/>},<br/>"resultCode": "SUCCESS",<br/>"resultMessage": "성공"<br/>} |
| 7 | {<br/>'Content-Type': 'application/json',<br/>'Authorization':'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0QG5hdmVyL.....'<br/>} | {<br/>"data": null,<br/>"resultCode": "SUCCESS",<br/>"resultMessage": "성공"<br/>}  |
| 8 | {<br/>'Content-Type': 'application/json',<br/>'Authorization':'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0QG5hdmVyL.....'<br/>} | {<br/>"data": null,<br/>"resultCode": "SUCCESS",<br/>"resultMessage": "성공"<br/>} |
| 9 | {<br/>'Content-Type': 'application/json',<br/>'Authorization':'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0QG5hdmVyL.....'<br/>} |  {<br/>"data": null,<br/>"resultCode": "SUCCESS",<br/>"resultMessage": "성공"<br/>} |

<br/>

># 개발 Overview
기존부터 만들어보고 싶었던 프로젝트 중 부동산 플랫폼을 진행하고자 합니다.<br/>
개발서버는 개인 NAS에 구축하고 실 서비스 할 서버는 컴퓨터 하나를 포멧 후 Linux를 설치해서 사용하기로 했습니다. <br/>
Server 및 DB 프로젝트 구성이 어느정도 되어 toady.co.kr 도메인을 구매하여 SSL인증서를 적용습니다. <br/>
사용자에게 서비스 할 화면이 필요하기 때문에 우선 웹 Front는 React로 환경 구성 및 구조를 잡아놓고 간간히 개발 중이며 임시적으로 구성 뒤 UI는 퍼블리셔에게 의뢰할 예정입나다.<br/>
청약 정보는 API로 제공되는곳이 보이지 않아서 청약홈 일정을 파싱해서 주기적으로 Spring Batch를 통해 DB에 반영하도록 했습니다.<br/>
